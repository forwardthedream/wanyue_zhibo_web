<?php
// +----------------------------------------------------------------------
// | 短信配置
// +----------------------------------------------------------------------

return [
    //默认支付模式
    'default' => 'yunxin',
    //单个手机每日发送上限
    'maxPhoneCount' => 10,
    //验证码每分钟发送上线
    'maxMinuteCount' => 20,
    //单个IP每日发送上限
    'maxIpCount' => 50,
    //驱动模式
    'stores' => [
        //云信
        'yunxin' => [
            //短信模板id
            'template_id' => [
                //验证码
                'VERIFICATION_CODE' => 518076,
                //支付成功
                'PAY_SUCCESS_CODE' => 520268,
                //发货提醒
                'DELIVER_GOODS_CODE' => 520269,
                //确认收货提醒
                'TAKE_DELIVERY_CODE' => 520271,
                //管理员下单提醒
                'ADMIN_PLACE_ORDER_CODE' => 520272,
                //管理员退货提醒
                'ADMIN_RETURN_GOODS_CODE' => 520274,
                //管理员支付成功提醒
                'ADMIN_PAY_SUCCESS_CODE' => 520273,
                //管理员确认收货
                'ADMIN_TAKE_DELIVERY_CODE' => 520422,
                //改价提醒
                'PRICE_REVISION_CODE' => 528288,
                //订单未支付
                'ORDER_PAY_FALSE' => 528116,
            ],
        ],
        //阿里云
        'aliyun' => [
            'template_id' => [

            ]
        ],
        //腾讯云
        'tencent' => [
            //API密钥配置 (密钥查询：https://console.cloud.tencent.com/cam/capi)
            'secret_id' => '',
            'secret_key' => '',
            //应用配置
            'sdk_app_id' => '',//SDK AppID是短信应用的唯一标识
            'app_key' => '',//App Key是用来校验短信发送合法性的密码，与SDK AppID对应
            //短信签名
            'signature' => '万岳电商系统',
            //短信模板
            'template_id' => [
                //通用验证码 √
                'VERIFICATION_CODE' => 67330,
                //支付成功提醒 √
                'PAY_SUCCESS_CODE' => 67572,
                //发货提醒 √
                'DELIVER_GOODS_CODE' => 67108,
                //确认收货提醒 √
                'TAKE_DELIVERY_CODE' => 67319,
                //管理员下单提醒
                'ADMIN_PLACE_ORDER_CODE' => 67116,
                //管理员退货提醒 √
                'ADMIN_RETURN_GOODS_CODE' => 63126,
                //管理员支付成功通知 √
                'ADMIN_PAY_SUCCESS_CODE' => 67318,
                //管理员确认收货通知 √
                'ADMIN_TAKE_DELIVERY_CODE' => 67312,
                //改价提醒 √
                'PRICE_REVISION_CODE' => 67322,
                //提醒付款通知
                'ORDER_PAY_FALSE' => 73121,
            ],
        ]
    ]
];