

> <div align=center><img src="https://images.gitee.com/uploads/images/2021/1018/173731_d689ecb0_8162876.png" width="590" height="212"/></div>
> 
> 
> ### 项目说明（如果对你有用，请给个star！）
> ##### <a target="_blank" href="https://www.kancloud.cn/wanyuekeji/wanyue_open_zhibo/2479716">项目文档</a> |  <a target="_blank" href="https://www.kancloud.cn/wanyuekeji/wanyue_open_zhibo/2479717">部署文档</a>  |  <a target="_blank" href="https://www.kancloud.cn/wanyuekeji/wanyue_open_zhibo/2479722">常见问题</a> 
> ---
> 
> ### 系统演示
> - 总后台地址: <a target="_blank" href="https://malldemo.sdwanyue.com/admin/login/index">https://malldemo.sdwanyue.com/admin/login/index</a> 账号：visitor     密码：visitor
> - 商户后台地址: <a target="_blank" href="https://malldemo.sdwanyue.com/shop/index">https://malldemo.sdwanyue.com/shop/index</a> 账号：15711449029 验证码：123456（点击获取验证码后，输入该验证码即可）
> 
> ### 演示APP
> ![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/201432_b98e1d63_8162876.png "155114_9bce1969_8162876.png")
> 
> ### 用户端仓库地址（可直接跳转查看）
>  - ios版仓库地址: <a target="_blank" href="https://gitee.com/WanYueKeJi/wanyue_zhibo_ios">点击此处</a>
>  - android仓库地址: <a target="_blank" href="https://gitee.com/WanYueKeJi/wanyue_zhibo_android">点击此处</a>
> 
>    
> ### 项目介绍
> 万岳科技可为商家快速搭建起一套属于自己的直播商城系统，有效避开商城直播过程中的痛点难点，加入自身创意的同时，汲取各家平台的特色功能和体验，并且可根据用户的运营需求对系统做定制开发。
> * 所有使用到的框架或者组件都是基于开源项目,代码保证100%开源。
> * 系统功能通用，无论是个人还是企业都可以利用该系统快速搭建一个属于自己的直播商城系统。
> 
> 
> ### 功能简介
![输入图片说明](https://images.gitee.com/uploads/images/2021/1101/173044_ae046c7e_8162876.png "直播电商开源版1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1101/173055_84f488a5_8162876.png "直播电商开源版2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1101/173103_5095e764_8162876.png "直播电商开源版3.png")
>  ### 功能介绍表
> ![输入图片说明](210122_47ed5487_8162876.png)
>  ### 开源版使用须知
>     
>    - 允许用于个人学习、教学案例
>    - 开源版不适合商用，商用请购买商业版
>    - 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负
>             
>   
> ###  开源交流群【加群回答请填写“gitee直播”，免费领取数据库文件】
>   咨询了解QQ：2770722087  微信：wanyuejiaoyu123
> > QQ群：995910672
> 
<img class="kefu_weixin" style="float:left;" src="https://images.gitee.com/uploads/images/2021/0524/181101_c6bda503_2242923.jpeg" width="102" height="102"/>
> 
