<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>系统配置</title>

    <link href="{__FRAME_PATH}/css/bootstrap.min.css?v=3.4.0" rel="stylesheet">
    <link href="{__ADMIN_PATH}/css/layui-admin.css" rel="stylesheet">
    <link href="{__FRAME_PATH}/css/style.min.css?v=3.0.0" rel="stylesheet">
    <link href="{__FRAME_PATH}css/font-awesome.min.css?v=4.3.0" rel="stylesheet">
    <script src="{__PLUG_PATH}vue/dist/vue.min.js"></script>
    <link href="{__PLUG_PATH}iview/dist/styles/iview.css" rel="stylesheet">
    <script src="{__PLUG_PATH}iview/dist/iview.min.js"></script>
    <script src="{__PLUG_PATH}jquery/jquery.min.js"></script>
    <script src="{__PLUG_PATH}form-create/province_city.js"></script>
    <script src="{__PLUG_PATH}form-create/form-create.min.js"></script>
    <link href="{__PLUG_PATH}layui/css/layui.css" rel="stylesheet">
    <script src="{__PLUG_PATH}layui/layui.all.js"></script>
    <style>
        /*弹框样式修改*/
        .ivu-modal{top: 20px;}
        .ivu-modal .ivu-modal-body{padding: 10px;}
        .ivu-modal .ivu-modal-body .ivu-modal-confirm-head{padding:0 0 10px 0;}
        .ivu-modal .ivu-modal-body .ivu-modal-confirm-footer{display: none;padding-bottom: 10px;}
        .ivu-date-picker {display: inline-block;line-height: normal;width: 280px;}
        .ivu-modal-footer{display: none;}
        .ivu-poptip-popper{text-align: left;display: none;position: absolute; will-change: top, left; top: -37px; left: 85px;}
        .ivu-icon{padding-left: 5px;}
        .ivu-btn-long{width: 10%;min-width:100px;margin-left: 18%;}
        .ivu-poptip:hover .ivu-poptip-popper{
            display: block;
        }
    </style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="tabs-container ibox-title  gray-bg">

                    <div class="ibox-content">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="javascript:void(0)"><i class="fa fa-cog"></i>基础配置</a>
                            </li>
                        </ul>
                        <div class="p-m m-t-sm" id="configboay">
                            <form autocomplete="off" class="ivu-form ivu-form-label-right form-create" id="confrom" name="confrom">
                                <div class="ivu-row">
                                    <div class="ivu-col ivu-col-span-13">
                                        <div class="ivu-form-item">
                                            <label class="ivu-form-item-label" style="width: 125px;"><span>客服链接
                                                <div class="ivu-poptip">
                                                    <div class="ivu-poptip-rel">
                                                        <i class="ivu-icon ivu-icon-ios-information-outline" style="font-size: 16px;"></i>
                                                    </div>
                                                    <div class="ivu-poptip-popper" style="" x-placement="top-start">
                                                        <div class="ivu-poptip-content">
                                                            <div class="ivu-poptip-arrow">
                                                            </div>
                                                            <div class="ivu-poptip-inner">
                                                                <div class="ivu-poptip-body">
                                                                    <div class="ivu-poptip-body-content">
                                                                        <div class="ivu-poptip-body-content-inner">
                                                                            注册链接：http://www.53kf.com/reg/index?yx_from=210260
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </span>
                                            </label>
                                            <div class="ivu-form-item-content" style="margin-left: 125px;">
                                                <div class="ivu-input-wrapper ivu-input-type">
                                                    <i class="ivu-icon ivu-icon-load-c ivu-load-loop ivu-input-icon ivu-input-icon-validate"></i>
                                                    <input autocomplete="off" spellcheck="false" type="text"
                                                           placeholder="注册链接：http://www.53kf.com/reg/index?yx_from=210260"
                                                           class="ivu-input" name="service_url" value="{$config['service_url']|default=''}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ivu-col ivu-col-span-24">
                                        <div class="ivu-col ivu-col-span-24">
                                            <button type="button" class="ivu-btn ivu-btn-primary ivu-btn-long ivu-btn-large" id="config_sub">
                                                <i class="ivu-icon ivu-icon-ios-upload"></i><span>提交</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script>
    (function () {
        var issub=0;
        $('#config_sub').click(function () {
            if(issub){
                return !1;
            }
            var data_a = $('#confrom').serializeArray();
            var nums=data_a.length;
            var data={};
            for(var i=0;i<nums;i++){
                var v=data_a[i];
                data[v.name]=v.value;
            }
            console.log(data);
            issub=1;
            $.ajax({
                url:'{:Url('setting.systemConfig/update_config')}',
                type:'POST',
                data:data,
                dataType:'json',
                error:function (e) {
                    issub=0;
                    layer.msg('网络错误');
                },
                success:function (data) {
                    issub=0;
                    layer.msg(data.msg);
                }
            })
        })
    })();
</script>
</html>