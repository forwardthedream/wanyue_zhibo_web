<?php
namespace app\api\controller\user;

use app\models\user\UserAccount;
use app\Request;
use wanyue\services\UtilService;

/**
 * 提现账号
 */
class UserAccountController
{

    /**
     * 列表
     */
    public function lst(Request $request)
    {
        list($page, $limit) = UtilService::getMore([
            [['page', 'd'], 1],
            [['limit', 'd'], 20]
        ], $request, true);


        $uid=$request->uid();

        $list=UserAccount::getList($uid,$page,$limit);

        return app('json')->successful($list);
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        list($type,$name, $account, $bank) = UtilService::postMore([
            [['type', 'd'], 0],
            ['name', ''],
            ['account', ''],
            ['bank', ''],
        ], $request, true);

        if($type==1){
            if($account=='')  return app('json')->fail('请输入支付宝账号');
            if($name=='')  return app('json')->fail('请输入姓名');
        }

        if($type==2){
            if($account=='')  return app('json')->fail('请输入微信账号');
        }

        if($type==3){
            if($bank=='')  return app('json')->fail('请输入银行名称');
            if($account=='')  return app('json')->fail('请输入银行卡号');
            if($name=='')  return app('json')->fail('请输入持卡人姓名');

        }

        $uid=$request->uid();

        $res=UserAccount::setAccount($uid,$type,$account,$name,$bank);

        if(!$res) return app('json')->fail(UserAccount::getErrorInfo());

        return app('json')->successful('添加成功',['id'=>$res['id']]);
    }

    /**
     * 关注
     */
    public function edit(Request $request)
    {
        list($accountid,$type,$name, $account, $bank) = UtilService::postMore([
            [['accountid', 'd'], 0],
            [['type', 'd'], 0],
            ['name', ''],
            ['account', ''],
            ['bank', ''],
        ], $request, true);

        if($type==1){
            if($account=='')  return app('json')->fail('请输入支付宝账号');
            if($name=='')  return app('json')->fail('请输入姓名');
        }

        if($type==2){
            if($account=='')  return app('json')->fail('请输入微信账号');
        }

        if($type==3){
            if($bank=='')  return app('json')->fail('请输入银行名称');
            if($account=='')  return app('json')->fail('请输入银行卡号');
            if($name=='')  return app('json')->fail('请输入持卡人姓名');

        }

        $uid=$request->uid();

        $res=UserAccount::editAttent($uid,$accountid,$type,$account,$name,$bank);

        if (!$res) return app('json')->fail(UserAccount::getErrorInfo());

        return app('json')->successful('修改成功');
    }

    //取消关注
    public function del(Request $request)
    {
        list($accountid) = UtilService::postMore([
            [['accountid', 'd'], 0],
        ], $request, true);


        $uid=$request->uid();

        $where=[
            'uid'=>$uid,
            'id'=>$accountid,
        ];

        $res=UserAccount::delAttent($where);

        return app('json')->successful('删除成功');
    }

}