<?php

namespace app\api\controller\order;

use app\admin\model\system\{
    SystemAttachment, ShippingTemplates
};

use wanyue\repositories\OrderRepository;
use app\models\store\{
    StoreCart,
    StoreOrder,
    StoreOrderCartInfo,
    StoreProductReply
};
use app\models\user\User as apiUser;
use app\models\user\UserAddress;
use app\Request;
use wanyue\services\{
    CacheService,
    ExpressService,
    SystemConfigService,
    UtilService
};

/**
 * 订单类
 * Class StoreOrderController
 * @package app\api\controller\order
 */
class StoreOrderController
{
    /**
     * 订单确认
     * @param Request $request
     * @return mixed
     */
    public function confirm(Request $request)
    {
        $temp = ShippingTemplates::get(1);
        if (!$temp) return app('json')->fail('默认模板未配置，无法下单');
        list($cartId) = UtilService::postMore(['cartId'], $request, true);
        if (!is_string($cartId) || !$cartId) return app('json')->fail('请提交购买的商品');
        $uid = $request->uid();
        $cartGroup = StoreCart::getUserProductCartList($uid, $cartId, 1);
        if (count($cartGroup['invalid'])) return app('json')->fail($cartGroup['invalid'][0]['productInfo']['store_name'] . '已失效!');
        if (!$cartGroup['valid']) return app('json')->fail('请提交购买的商品');
        $cartInfo_shop = $cartGroup['valid'];
        $addr = UserAddress::where('uid', $uid)->where('is_default', 1)->find();

        $total_price=0;
        $pay_postage=0;
        //根据店铺处理信息
        foreach ($cartInfo_shop as $k=>$v){
            $cartInfo=$v['list'];
            $priceGroup = StoreOrder::getOrderPriceGroup($cartInfo, $addr);
            if ($priceGroup === false) {
                return app('json')->fail(StoreOrder::getErrorInfo('运费模板不存在'));
            }

            $total_price=bcadd($total_price,$priceGroup['totalPrice'],2);
            $pay_postage=bcadd($pay_postage,$priceGroup['storePostage'],2);

            $usableCoupon = null;

            $v['usableCoupon']=$usableCoupon;
            $v['deduction'] = false;

            $v['priceGroup'] = $priceGroup;

            $cartInfo_shop[$k]=$v;
        }

        $pay_price=bcadd($total_price,$pay_postage,2);
        $data['total_price']=$total_price;
        $data['pay_postage']=$pay_postage;
        $data['pay_price']=$pay_price;

        $other = [
            'offlinePostage' => sys_config('offline_postage'),
            'integralRatio' => sys_config('integral_ratio')
        ];


        $data['addressInfo'] = UserAddress::getUserDefaultAddress($uid);


        $data['cartInfo'] = $cartInfo_shop;


        $data['orderKey'] = StoreOrder::cacheOrderInfo($uid, $cartInfo_shop, $priceGroup, $other);
        $data['offlinePostage'] = $other['offlinePostage'];

        $user = apiUser::getUserInfo($request->uid());
        if (isset($user['pwd'])) unset($user['pwd']);
        $data['userInfo'] = $user;

        $data['integralRatio'] = $other['integralRatio']; //积分抵用比例

        return app('json')->successful($data);
    }

    /**
     * 计算订单金额
     * @param Request $request
     * @param $key
     * @return mixed
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function computedOrder(Request $request, $key)
    {

//        $priceGroup = StoreOrder::getOrderPriceGroup($cartInfo);
        if (!$key) return app('json')->fail('参数错误!');
        $uid = $request->uid();
        if (StoreOrder::be(['order_id|unique' => $key, 'uid' => $uid, 'is_del' => 0]))
            return app('json')->status('extend_order', '订单已生成', ['orderId' => $key, 'key' => $key]);
        list($addressId, $payType, $useIntegral, $mark,  $formId, $shipping_type) = UtilService::postMore([
            'addressId', ['payType', 'yue'], ['useIntegral', 0], 'mark', ['formId', ''],
            ['shipping_type', 1],
        ], $request, true);
        $payType = strtolower($payType);


        $priceGroup = StoreOrder::cacheKeyCreateOrder($request->uid(), $key, $addressId, $payType, (int)$useIntegral, $mark, true, 0, $shipping_type);
        if ($priceGroup)
            return app('json')->status('NONE', 'ok', $priceGroup);
        else
            return app('json')->fail(StoreOrder::getErrorInfo('计算失败'));
    }

    /**
     * 订单创建
     * @param Request $request
     * @param $key
     * @return mixed
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function create(Request $request, $key)
    {
        if (!$key) return app('json')->fail('参数错误!');
        $uid = $request->uid();

        if (StoreOrder::be(['order_id|unique' => $key, 'uid' => $uid, 'is_del' => 0]))
            return app('json')->status('extend_order', '订单已生成', ['orderId' => $key, 'key' => $key]);
        list($addressId, $payType, $useIntegral, $mark,  $formId, $from, $shipping_type, $real_name, $phone, $storeId) = UtilService::postMore([
            'addressId', 'payType', ['useIntegral', 0], 'mark',  ['formId', ''],  ['from', 'weixin'],
            ['shipping_type', 1], ['real_name', ''], ['phone', ''], ['store_id', 0]
        ], $request, true);
        $payType = strtolower($payType);

        $isChannel = 1;
        if ($from == 'weixin')
            $isChannel = 0;
        elseif ($from == 'weixinh5')
            $isChannel = 2;
        elseif ($from == 'android')
            $isChannel = 3;
        elseif ($from=='ios')
            $isChannel = 4;
        $order_a = StoreOrder::cacheKeyCreateOrder($request->uid(), $key, $addressId, $payType, (int)$useIntegral, $mark,  false, $isChannel, $shipping_type, $real_name, $phone, $storeId);
        if ($order_a === false) return app('json')->fail(StoreOrder::getErrorInfo('订单生成失败'));
        $orders=$order_a['orders'];
        $pay_price=$order_a['pay_price'];
        $orderIds=$order_a['order_id2'];

        $orderId=$orders[0]['order_id'];
        $key=$orders[0]['unique'];

        $info = compact('orderId', 'key');
        if ($orders) {
            foreach ($orders as $k=>$v){
                event('OrderCreated', [$v]); //订单创建成功事件
            }

//            event('ShortMssageSend', [$orderId, 'AdminPlaceAnOrder']);//发送管理员通知
            switch ($payType) {
                case "weixin":
                    $res=false;
                    foreach ($orders as $k=>$v){
                        $orderInfo=$v;
                        $orderId=$orderInfo['order_id'];
                        //支付金额为0
                        if (bcsub((float)$orderInfo['pay_price'], 0, 2) <= 0) {
                            //创建订单jspay支付
                            $payPriceStatus = StoreOrder::jsPayPrice($orderId, $uid, $formId);
                            if ($payPriceStatus)//0元支付成功
                                $res=true;
                            else
                                return app('json')->status('pay_error', StoreOrder::getErrorInfo());
                        }
                    }
                    if($pay_price>0){
                        try {

                                $jsConfig = OrderRepository::AppPayShop($uid, $orderIds,$pay_price);

                        } catch (\Exception $e) {
                            return app('json')->status('pay_error', $e->getMessage(), $info);
                        }
                        $info['jsConfig'] = $jsConfig;
                        if ($from == 'weixinh5') {
                            return app('json')->status('wechat_h5_pay', '订单创建成功', $info);
                        } else if ($from == 'android' || $from=='ios') {
                            return app('json')->status('wechat_app', '订单创建成功', $info);
                        }else {
                            return app('json')->status('wechat_pay', '订单创建成功', $info);
                        }
                    }else{
                        return app('json')->status('success', '微信支付成功', $info);
                    }
                    
                    break;
                case 'yue':
                    $res=false;
                    foreach ($orders as $k=>$v){
                        $orderId=$v['order_id'];
                        if (StoreOrder::yuePay($orderId, $request->uid(), $formId))
                            $res=true;
                        else {
                            $errorinfo = StoreOrder::getErrorInfo();
                            if (is_array($errorinfo))
                                return app('json')->status($errorinfo['status'], $errorinfo['msg'], $info);
                            else
                                return app('json')->status('pay_error', $errorinfo);
                        }
                    }

                    if($res){
                        return app('json')->status('success', '余额支付成功', $info);
                    }
                    break;
                case 'offline':
                    return app('json')->status('success', '订单创建成功', $info);
                    break;
            }
        } else return app('json')->fail(StoreOrder::getErrorInfo('订单生成失败!'));
    }

    /**
     * 订单 再次下单
     * @param Request $request
     * @return mixed
     */
    public function again(Request $request)
    {
        list($uni) = UtilService::postMore([
            ['uni', ''],
        ], $request, true);
        if (!$uni) return app('json')->fail('参数错误!');
        $order = StoreOrder::getUserOrderDetail($request->uid(), $uni);
        if (!$order) return app('json')->fail('订单不存在!');
        $order = StoreOrder::tidyOrder($order, true);
        $res = [];
        foreach ($order['cartInfo'] as $v) {

            $res[] = StoreCart::setCart($request->uid(), $v['product_id'], $v['cart_num'], isset($v['productInfo']['attrInfo']['unique']) ? $v['productInfo']['attrInfo']['unique'] : '', 'product', 1, 0);
        }
        $cateId = [];
        foreach ($res as $v) {
            if (!$v) return app('json')->fail('再来一单失败，请重新下单!');
            $cateId[] = $v['id'];
        }
        event('OrderCreateAgain', implode(',', $cateId));
        return app('json')->successful('ok', ['cateId' => implode(',', $cateId)]);
    }


    /**
     * 订单支付
     * @param Request $request
     * @return mixed
     */
    public function pay(Request $request)
    {
        list($uni, $paytype, $from) = UtilService::postMore([
            ['uni', ''],
            ['paytype', 'weixin'],
            ['from', 'weixin']
        ], $request, true);
        if (!$uni) return app('json')->fail('参数错误!');
        $order = StoreOrder::getUserOrderDetail($request->uid(), $uni);
        if (!$order)
            return app('json')->fail('订单不存在!');
        if ($order['paid'])
            return app('json')->fail('该订单已支付!');


        if ($paytype == 'weixin') {
            $order['order_id'] = mt_rand(100, 999) . '_' . $order['order_id'];
        }
        $order['pay_type'] = $paytype; //重新支付选择支付方式
        switch ($order['pay_type']) {
            case 'weixin':
                try {

                        $jsConfig = OrderRepository::AppPay($order);

                } catch (\Exception $e) {
                    return app('json')->fail($e->getMessage());
                }

                return app('json')->status('wechat_pay', ['jsConfig' => $jsConfig, 'order_id' => $order['order_id']]);

                break;
            case 'yue':
                if (StoreOrder::yuePay($order['order_id'], $request->uid()))
                    return app('json')->status('success', '余额支付成功');
                else {
                    $error = StoreOrder::getErrorInfo();
                    return app('json')->fail(is_array($error) && isset($error['msg']) ? $error['msg'] : $error);
                }
                break;
            case 'offline':

                if (StoreOrder::setOrderTypePayOffline($order['order_id']))
                    return app('json')->status('success', '订单创建成功');
                else
                    return app('json')->status('success', '支付失败');
                break;
        }
        return app('json')->fail('支付方式错误');
    }

    /**
     * 订单列表
     * @param Request $request
     * @return mixed
     */
    public function lst(Request $request)
    {
        list($type, $page, $limit, $search,$status) = UtilService::getMore([
            ['type', ''],
            ['page', 0],
            ['limit', 20],
            ['search', ''],
            ['status', '0'],
        ], $request, true);
        $uid=$request->uid();

        return app('json')->successful(StoreOrder::getUserOrderSearchList($uid, $type, $page, $limit, $search,$status));
    }

    /**
     * 订单详情
     * @param Request $request
     * @param $uni
     * @return mixed
     */
    public function detail(Request $request, $uni)
    {
        if (!strlen(trim($uni))) return app('json')->fail('参数错误');

        list($status) = UtilService::getMore([
            ['status', '0'],
        ], $request, true);

        $uid=$request->uid();

        $order = StoreOrder::getUserOrderDetail($uid, $uni, $status);

        if (!$order) return app('json')->fail('订单不存在');
        $order = $order->toArray();
        //是否开启门店自提
        $store_self_mention = sys_config('store_self_mention');
        //关闭门店自提后 订单隐藏门店信息
        if ($store_self_mention == 0) $order['shipping_type'] = 1;
        if ($order['verify_code']) {
            $verify_code = $order['verify_code'];
            $verify[] = substr($verify_code, 0, 4);
            $verify[] = substr($verify_code, 4, 4);
            $verify[] = substr($verify_code, 8);
            $order['_verify_code'] = implode(' ', $verify);
        }
        $order['add_time_y'] = date('Y-m-d', $order['add_time']);
        $order['add_time_h'] = date('H:i:s', $order['add_time']);
        if ($order['shipping_type'] === 2 && $order['verify_code']) {
            $name = $order['verify_code'] . '.jpg';
            $imageInfo = SystemAttachment::getInfo($name, 'name');
            $siteUrl = sys_config('site_url');
            if (!$imageInfo) {
                $imageInfo = UtilService::getQRCodePath($order['verify_code'], $name);
                if (is_array($imageInfo)) {
                    SystemAttachment::attachmentAdd($imageInfo['name'], $imageInfo['size'], $imageInfo['type'], $imageInfo['dir'], $imageInfo['thumb_path'], 1, $imageInfo['image_type'], $imageInfo['time'], 2);
                    $url = $imageInfo['dir'];
                } else
                    $url = '';
            } else $url = $imageInfo['att_dir'];
            if (isset($imageInfo['image_type']) && $imageInfo['image_type'] == 1) $url = $siteUrl . $url;
            $order['code'] = $url;
        }
        $order['mapKey'] = sys_config('tengxun_map_key');
        return app('json')->successful('ok', StoreOrder::tidyOrder($order, true, true));
    }

    /**
     * 订单删除
     * @param Request $request
     * @return mixed
     */
    public function del(Request $request)
    {
        list($uni) = UtilService::postMore([
            ['uni', ''],
        ], $request, true);
        if (!$uni) return app('json')->fail('参数错误!');
        $res = StoreOrder::removeOrder($uni, $request->uid());
        if ($res)
            return app('json')->successful();
        else
            return app('json')->fail(StoreOrder::getErrorInfo());
    }

    /**
     * 订单收货
     * @param Request $request
     * @return mixed
     */
    public function take(Request $request)
    {
        list($uni) = UtilService::postMore([
            ['uni', ''],
        ], $request, true);
        if (!$uni) return app('json')->fail('参数错误!');
        $res = StoreOrder::takeOrder($uni, $request->uid());
        if ($res) {
            $order_info = StoreOrder::where('order_id', $uni)->find();
            $gain_integral = intval($order_info['gain_integral']);

            return app('json')->successful('收货成功',['gain_integral' => $gain_integral, 'gain_coupon' => 0]);
        } else
            return app('json')->fail(StoreOrder::getErrorInfo());
    }


    /**
     * 订单 查看物流
     * @param Request $request
     * @param $uni
     * @return mixed
     */
    public function express(Request $request, $uni)
    {
        if (!$uni || !($order = StoreOrder::getUserOrderDetail($request->uid(), $uni))) return app('json')->fail('查询订单不存在!');
        if ($order['delivery_type'] != 'express' || !$order['delivery_id']) return app('json')->fail('该订单不存在快递单号!');
        $cacheName = $uni . $order['delivery_id'];
        $result = CacheService::get($cacheName, null);
        if ($result === NULL) {
            $result = ExpressService::query($order['delivery_id']);
            if (is_array($result) &&
                isset($result['result']) &&
                isset($result['result']['deliverystatus']) &&
                $result['result']['deliverystatus'] >= 3)
                $cacheTime = 0;
            else
                $cacheTime = 1800;
            CacheService::set($cacheName, $result, $cacheTime);
        }
        $orderInfo = [];
        $cartInfo = StoreOrderCartInfo::where('oid', $order['id'])->column('cart_info', 'unique') ?? [];
        $info = [];
        $cartNew = [];
        foreach ($cartInfo as $k => $cart) {
            $cart = json_decode($cart, true);
            $cartNew['cart_num'] = $cart['cart_num'];
            $cartNew['truePrice'] = $cart['truePrice'];
            $cartNew['productInfo']['image'] = $cart['productInfo']['image'];
            $cartNew['productInfo']['store_name'] = $cart['productInfo']['store_name'];
            $cartNew['productInfo']['unit_name'] = $cart['productInfo']['unit_name'] ?? '';
            array_push($info, $cartNew);
            unset($cart);
        }
        $orderInfo['delivery_id'] = $order['delivery_id'];
        $orderInfo['delivery_name'] = $order['delivery_name'];
        $orderInfo['delivery_type'] = $order['delivery_type'];
        $orderInfo['cartInfo'] = $info;
        return app('json')->successful(['order' => $orderInfo, 'express' => $result ? $result : []]);
    }

    /**
     * 订单评价
     * @param Request $request
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function comment(Request $request)
    {
        $group = UtilService::postMore([
            ['unique', ''], ['comment', ''], ['pics', ''], ['product_score', 5], ['service_score', 5]
        ], $request);
        $unique = $group['unique'];
        unset($group['unique']);
        if (!$unique) return app('json')->fail('参数错误!');
        $cartInfo = StoreOrderCartInfo::where('unique', $unique)->find();
        $uid = $request->uid();
        $nickname='已删除';
        $avatar='';
        $user_info = \app\models\user\User::getUserInfoByRedis($uid);
        if($user_info){
            $nickname=$user_info['nickname'];
            $avatar=$user_info['avatar'];
        }
        $group['nickname'] = $nickname;
        $group['avatar'] = $avatar;
        if (!$cartInfo) return app('json')->fail('评价产品不存在!');
        $orderUid = StoreOrder::getOrderInfo($cartInfo['oid'], 'uid')['uid'];
        if ($uid != $orderUid) return app('json')->fail('评价产品不存在!');
        if (StoreProductReply::be(['oid' => $cartInfo['oid'], 'unique' => $unique]))
            return app('json')->fail('该产品已评价!');
        $group['comment'] = htmlspecialchars(trim(filter_emoji($group['comment'])));
        if ($group['product_score'] < 1) return app('json')->fail('请为产品评分');
        else if ($group['service_score'] < 1) return app('json')->fail('请为商家服务评分');
        $productId = $cartInfo['product_id'];
        if ($group['pics']){
            $group['pics'] = json_encode(is_array($group['pics']) ? $group['pics'] : explode(',', $group['pics']));
        }else{
            $group['pics']='';
        }
        $mer_id= isset($cartInfo['cart_info']['productInfo']['mer_id']) ? $cartInfo['cart_info']['productInfo']['mer_id'] : 0;
        $group = array_merge($group, [
            'uid' => $uid,
            'oid' => $cartInfo['oid'],
            'mer_id' => $mer_id,
            'unique' => $unique,
            'product_id' => $productId,
            'add_time' => time(),
            'reply_type' => 'product'
        ]);
        StoreProductReply::beginTrans();
        $res = StoreProductReply::reply($group, 'product');
        if (!$res) {
            StoreProductReply::rollbackTrans();
            return app('json')->fail('评价失败!');
        }
        try {
            StoreOrder::checkOrderOver($cartInfo['oid']);
        } catch (\Exception $e) {
            StoreProductReply::rollbackTrans();
            return app('json')->fail($e->getMessage());
        }
        StoreProductReply::commitTrans();
        event('UserCommented', $res);
        event('AdminNewPush');
        return app('json')->successful();
    }

    /**
     * 订单统计数据
     * @param Request $request
     * @return mixed
     */
    public function data(Request $request)
    {
        list($status) = UtilService::getMore([
            ['status', '0'],
        ], $request, true);

        return app('json')->successful(StoreOrder::getOrderData($request->uid(),$status));
    }

    /**
     * 订单退款理由
     * @return mixed
     */
    public function refund_reason()
    {
        $reason = sys_config('stor_reason') ?: [];//退款理由
        $reason = str_replace("\r\n", "\n", $reason);//防止不兼容
        $reason = explode("\n", $reason);
        return app('json')->successful($reason);
    }

    /**
     * 订单退款审核
     * @param Request $request
     * @return mixed
     */
    public function refund_verify(Request $request)
    {
        $data = UtilService::postMore([
            ['text', ''],
            ['refund_reason_wap_img', ''],
            ['refund_reason_wap_explain', ''],
            ['uni', '']
        ], $request);
        $uni = $data['uni'];
        unset($data['uni']);
        if ($data['refund_reason_wap_img']) $data['refund_reason_wap_img'] = explode(',', $data['refund_reason_wap_img']);
        if (!$uni || $data['text'] == '') return app('json')->fail('参数错误!');
        $res = StoreOrder::orderApplyRefund($uni, $request->uid(), $data['text'], $data['refund_reason_wap_explain'], $data['refund_reason_wap_img']);
        if ($res)
            return app('json')->successful('提交申请成功');
        else
            return app('json')->fail(StoreOrder::getErrorInfo());
    }


    /**
     * 订单取消   未支付的订单回退积分,回退优惠券,回退库存
     * @param Request $request
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function cancel(Request $request)
    {
        list($id) = UtilService::postMore([['id', 0]], $request, true);
        if (!$id) return app('json')->fail('参数错误');
        if (StoreOrder::cancelOrder($id, $request->uid()))
            return app('json')->successful('取消订单成功');
        return app('json')->fail(StoreOrder::getErrorInfo('取消订单失败'));
    }


    /**
     * 订单产品信息
     * @param Request $request
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function product(Request $request)
    {
        list($unique) = UtilService::postMore([['unique', '']], $request, true);
        if (!$unique || !StoreOrderCartInfo::be(['unique' => $unique]) || !($cartInfo = StoreOrderCartInfo::where('unique', $unique)->find())) return app('json')->fail('评价产品不存在!');
        $cartInfo = $cartInfo->toArray();
        $cartProduct = [];
        $cartProduct['cart_num'] = $cartInfo['cart_info']['cart_num'];
        $cartProduct['productInfo']['image'] = isset($cartInfo['cart_info']['productInfo']['image']) ? $cartInfo['cart_info']['productInfo']['image'] : '';
        $cartProduct['productInfo']['price'] = isset($cartInfo['cart_info']['productInfo']['price']) ? $cartInfo['cart_info']['productInfo']['price'] : 0;
        $cartProduct['productInfo']['store_name'] = isset($cartInfo['cart_info']['productInfo']['store_name']) ? $cartInfo['cart_info']['productInfo']['store_name'] : '';
        if (isset($cartInfo['cart_info']['productInfo']['attrInfo'])) {
            $cartProduct['productInfo']['attrInfo']['product_id'] = isset($cartInfo['cart_info']['productInfo']['attrInfo']['product_id']) ? $cartInfo['cart_info']['productInfo']['attrInfo']['product_id'] : '';
            $cartProduct['productInfo']['attrInfo']['suk'] = isset($cartInfo['cart_info']['productInfo']['attrInfo']['suk']) ? $cartInfo['cart_info']['productInfo']['attrInfo']['suk'] : '';
            $cartProduct['productInfo']['attrInfo']['price'] = isset($cartInfo['cart_info']['productInfo']['attrInfo']['price']) ? $cartInfo['cart_info']['productInfo']['attrInfo']['price'] : '';
            $cartProduct['productInfo']['attrInfo']['image'] = isset($cartInfo['cart_info']['productInfo']['attrInfo']['image']) ? $cartInfo['cart_info']['productInfo']['attrInfo']['image'] : '';
        }
        $cartProduct['product_id'] = isset($cartInfo['cart_info']['product_id']) ? $cartInfo['cart_info']['product_id'] : 0;
        $cartProduct['order_id'] = StoreOrder::where('id', $cartInfo['oid'])->value('order_id');
        return app('json')->successful($cartProduct);
    }

    /**
     * 首页获取未支付订单
     */
    public function get_noPay(Request $request)
    {
        return app('json')->successful(StoreOrder::getUserOrderSearchList($request->uid(), 0, 0, 0, ''));
    }

}