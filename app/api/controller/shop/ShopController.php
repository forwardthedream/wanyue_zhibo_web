<?php

namespace app\api\controller\shop;

use app\models\shop\ShopApply;
use app\models\store\StoreOrder;
use app\models\user\User;
use app\Request;
use wanyue\services\UtilService;

/**
 * 店铺
 */
class ShopController
{
    //店铺统计
    public function myshop(Request $request)
    {
        $uid=$request->uid();

        $counts=StoreOrder::getOrderDataShop($uid);
        $unshipped=$counts['unshipped_count'];
        $received=$counts['received_count'];
        $evaluated=$counts['evaluated_count'];

        $userinfo=User::getUserInfo($uid,'shop,shop_ok');

        $shop=$userinfo['shop'];
        $shop_ok=$userinfo['shop_ok'];
        $shop_no=StoreOrder::getUnSettle($uid,1);
        $shop_all=bcadd($shop_ok,$shop_no,2);

        $shop_t=StoreOrder::getTodaySettle($uid,1);

        return app('json')->successful(compact('unshipped','received','evaluated','shop_t','shop_all','shop_ok','shop_no'));

    }
    //申请店铺
    public function apply(Request $request)
    {

        list($realname, $tel, $cer_no, $cer_f, $cer_b, $cer_h, $business, $license, $other) = UtilService::postMore([
            ['realname', ''],
            ['tel', ''],
            ['cer_no', ''],
            ['cer_f', ''],
            ['cer_b', ''],
            ['cer_h', ''],
            ['business', ''],
            ['license', ''],
            ['other', '']
        ], $request, true);

        if ($realname ==='') return app('json')->fail('请输入姓名');
        if ($tel ==='') return app('json')->fail('请输入手机号');
        if ($cer_no ==='') return app('json')->fail('请输入身份证号');
        if ($cer_f ==='') return app('json')->fail('请上传证件正面照');
        if ($cer_b ==='') return app('json')->fail('请上传证件被面照');
        if ($cer_h ==='') return app('json')->fail('请上传证件手持照');
        if ($business ==='') return app('json')->fail('请上传营业执照');

        $res = ShopApply::setApplay($request->uid(), $realname, $tel, $cer_no, $cer_f, $cer_b, $cer_h, $business, $license, $other);

        if (!$res) return app('json')->fail(ShopApply::getErrorInfo());

        return app('json')->successful('申请已提交，请耐心等待');

    }

    //店铺状态
    public function applystatus(Request $request)
    {
        $uid=$request->uid();
        $status='-2';
        $reason='';
        $applyinfo=ShopApply::getInfo($uid);
        if($applyinfo){
            $status=$applyinfo['status'];
            $reason=$applyinfo['reason'];
        }

        return app('json')->successful(compact('status','reason'));

    }

}