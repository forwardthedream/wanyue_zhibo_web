<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/12/18
 */

namespace app\models\live;

use wanyue\basic\BaseModel;
use think\facade\Cache;
use wanyue\traits\ModelTrait;
use app\models\live\Shutlist;
use app\models\live\LiveManager;
use app\models\user\User;

/**
 * TODO 直播踢人Model
 * Class StoreCart
 * @package app\models\store
 */
class LiveKick extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'live_kick';

    use  ModelTrait;

    //踢出
    public static function kick($uid, $touid, $liveuid)
    {
		//查询权限
		$user_management=LiveManager::checkmanager($uid,$liveuid);
		$touser_management=LiveManager::checkmanager($touid,$liveuid);
		//if($uid!=$liveuid) return self::setErrorInfo('无权操作');
		if($user_management<=30||$touser_management>30) return self::setErrorInfo('无权操作');

        if(self::isKick($touid,$liveuid)) return self::setErrorInfo('对方已被踢出');

        $actuid=$uid;
        $uid=$touid;
        $add_time=time();
        $res=self::create(compact('uid','actuid','liveuid','add_time'));
        if(!$res) return self::setErrorInfo('操作失败');

        return true;
    }
    //是否已踢出
    public static function isKick($touid, $liveuid)
    {
        return self::be(['uid'=>$touid,'liveuid'=>$liveuid]) > 0 ? 1 : 0;
    }

    //删除用户
    public static function delKickuser($uid,$touid, $liveuid)
    {
		//查询权限
		$user_management=LiveManager::checkmanager($uid,$liveuid);
		//$touser_management=LiveManager::checkmanager($touid,$liveuid);
		//if($uid!=$liveuid) return self::setErrorInfo('无权操作');
		if($user_management<=30) return self::setErrorInfo('无权操作');

        $res=self::where(['uid'=>$touid,'liveuid'=>$liveuid])->delete();


        return true;
    }

    //删除
    public static function delKick($touid, $liveuid)
    {
        return self::where(['uid'=>$touid,'liveuid'=>$liveuid])->delete();
    }
    //获取列表
    public static function getlist($liveuid,$p)
    {
		$p=$p>1?$p:1;
		$num=20;
		$start=($p-1)*$num;

		$time =time();
	

		$list=self::where('liveuid',$liveuid)->limit($start,$num)->select()->toArray();
		foreach($list as $k =>$t){
			$userinfo = User::getUserInfo($t['uid']);
			$t['nickname']=$userinfo['nickname'];			
			$t['avatar']=$userinfo['avatar'];			
			$list[$k]=$t;
		}
		return $list;
    }



}