<?php

namespace app\appapi\model\article;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use think\facade\Session;

/**
 * Class SystemAdmin
 * @package app\admin\model\system
 */
class Page extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'article_post';

    use ModelTrait;


}