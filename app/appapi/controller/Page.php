<?php

namespace app\appapi\controller;

use app\appapi\controller\AuthController;
use app\appapi\model\article\Page as PageModel;
use app\Request;
use think\facade\Route as Url;
use wanyue\services\UtilService;

class Page extends AuthController
{
    /**
     * 列表页面
     * @return string
     * @throws \Exception
     */
    public function index()
    {
        //return $this->fetch();
    }

    /**
     * 添加页面
     * @return string
     * @throws \Exception
     */
    public function detail(Request $request)
    {

        list($id) = UtilService::getMore([
            [['id', 'd'], 1],
        ], $request, true);
        if($id<1)  return $this->failed('参数错误', Url::buildUrl('/')->suffix(false)->build());
        $data=PageModel::get($id);
        if(!$data) return $this->failed('该内容不存在', Url::buildUrl('/')->suffix(false)->build());

        $this->assign(compact('data'));
        return $this->fetch();
    }


}