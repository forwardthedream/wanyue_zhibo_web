<?php

namespace app\admin\controller\finance;

use app\admin\controller\AuthController;
use app\admin\model\user\{ UserRechargegift as UserRechargeModel};
use wanyue\services\{JsonService,
    UtilService as Util};

/**
 * 微信充值记录
 * Class UserRecharge
 * @package app\admin\controller\user
 */
class UserRechargegift extends AuthController
{
    /**
     * 显示操作记录
     */
    public function index()
    {
        $this->assign('year', get_month());
        return $this->fetch();
    }

    public function get_user_recharge_list()
    {
        $where = Util::getMore([
            ['data', ''],
            ['paid', ''],
            ['page', 1],
            ['limit', 20],
            ['nickname', ''],
            ['excel', ''],
        ]);
        return JsonService::successlayui(UserRechargeModel::getUserRechargeList($where));
    }

    public function delect($id = 0)
    {
        if (!$id) return JsonService::fail('缺少参数');
        $rechargInfo = UserRechargeModel::get($id);
        if ($rechargInfo->paid) return JsonService::fail('已支付的订单记录无法删除');
        if (UserRechargeModel::del($id))
            return JsonService::successful('删除成功');
        else
            return JsonService::fail('删除失败');
    }

    public function get_badge()
    {
        $where = Util::getMore([
            ['data', ''],
            ['paid', ''],
            ['nickname', ''],
        ]);
        return JsonService::successful(UserRechargeModel::getDataList($where));
    }
}
