<?php

namespace app\admin\controller\live;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\admin\model\live\Shut as Model;
use wanyue\services\{FormBuilder as Form, JsonService as Json, UtilService as Util};

/**
 * 禁言管理
 * Class StoreCategory
 * @package app\admin\controller\system
 */
class Shut extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /*
     *  异步获取列表
     *  @return json
     */
    public function getlist()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
            ['uid', 0],
            ['liveuid', 0],
        ]);
        return Json::successlayui(Model::getList($where));
    }


    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if (!Model::delid($id))
            return Json::fail(Model::getErrorInfo('删除失败,请稍候再试!'));
        else
            return Json::successful('删除成功!');
    }
}
