<?php

namespace app\admin\controller\live;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\admin\model\live\Liveing as LiveingModel;
use wanyue\services\{FormBuilder as Form, JsonService as Json, UtilService as Util};

/**
 * 产品分类控制器
 * Class StoreCategory
 * @package app\admin\controller\system
 */
class Liveing extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /*
     *  异步获取分类列表
     *  @return json
     */
    public function getlist()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
            ['order', '']
        ]);
        return Json::successlayui(LiveingModel::getList($where));
    }

}
