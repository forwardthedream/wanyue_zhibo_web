<?php

namespace app\admin\model\article;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use think\facade\Cache;

/**
 * Class StoreCategory
 * @package app\admin\model\store
 */
class Post extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'article_post';

    use ModelTrait;

    /**
     * 异步获取分类列表
     * @param $where
     * @return array
     */
    public static function getList($where)
    {
        $data = ($data = self::page((int)$where['page'], (int)$where['limit'])->select()) && count($data) ? $data->toArray() : [];
        foreach ($data as &$item) {
            $item['url']=set_file_url('/appapi/page/detail?id='.$item['id']);
        }
        $count = self::count();
        return compact('count', 'data');
    }


    public static function delid($id)
    {
        return self::del($id);
    }

}