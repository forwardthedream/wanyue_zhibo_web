<?php

namespace app\admin\model\live;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use app\Redis;

/**
 * Class ReportClass
 * @package app\admin\model\store
 */
class ReportClass extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'live_report_cat';

    use ModelTrait;

    /**
     * 异步获取分类列表
     * @param $where
     * @return array
     */
    public static function getList($where)
    {
        $data = ($data = self::page((int)$where['page'], (int)$where['limit'])->select()) && count($data) ? $data->toArray() : [];
        foreach ($data as &$item) {

        }
        $count = self::count();
        return compact('count', 'data');
    }


    public static function delid($id)
    {
        return self::del($id);
    }

    public static function upCache()
    {
        $key='live_report_cat';
        $list=self::field('id,name')->order('sort desc')->select()->toArray();
        if($list){
            Redis::set($key, $list);
        }else{
            Redis::del($key);
        }
        return 1;
    }

}