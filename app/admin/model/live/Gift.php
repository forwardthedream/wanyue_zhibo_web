<?php

namespace app\admin\model\live;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;

/**
 * Class StoreCategory
 * @package app\admin\model\store
 */
class Gift extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'gift';

    use ModelTrait;

    /**
     * 异步获取分类列表
     * @param $where
     * @return array
     */
    public static function getList($where)
    {
        $data = ($data = self::page((int)$where['page'], (int)$where['limit'])->select()) && count($data) ? $data->toArray() : [];
        foreach ($data as &$item) {

        }
        $count = self::count();
        return compact('count', 'data');
    }


    public static function delGift($id)
    {
        return self::del($id);
    }

    /**
     * 隐藏显示
     * @param $id
     * @param $show
     * @return bool
     */
    public static function setShow($id, $show)
    {
        $count = self::where('id', $id)->count();
        if (!$count) return self::setErrorInfo('参数错误');
        $count = self::where('id', $id)->where('is_show', $show)->count();
        if ($count) return true;

        self::beginTrans();
        $res = self::where('id', $id)->update(['is_show' => $show]);
        self::checkTrans($res);
        return $res;
    }
}