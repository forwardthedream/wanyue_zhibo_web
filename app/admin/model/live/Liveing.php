<?php

namespace app\admin\model\live;

use app\models\live\LiveClass;
use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use app\models\user\User;
use app\models\live\Live;

/**
 * Class StoreCategory
 * @package app\admin\model\store
 */
class Liveing extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'live';

    use ModelTrait;

    /**
     * 异步获取分类列表
     * @param $where
     * @return array
     */
    public static function getLiveWhere($where)
    {
        $model=new self;
        $model=$model->where('islive','1');
        if(isset($where['uid'])){
            $model=$model->where('uid',$where['uid']);
        }

        if(isset($where['isvideo'])){
            $model=$model->where('isvideo',$where['isvideo']);
        }

        return $model;
    }

    /**
     * 异步获取分类列表
     * @param $where
     * @return array
     */
    public static function getList($where)
    {
        $classlist=LiveClass::getList();

        $data = ($data = self::getLiveWhere($where)->page((int)$where['page'], (int)$where['limit'])->select()) && count($data) ? $data->toArray() : [];

        foreach ($data as &$item) {
            $nickname='';
            $avatar='';
            $userinfo=User::getUserInfoByRedis($item['uid']);
            if($userinfo){
                $nickname=$userinfo['nickname'];
                $avatar=$userinfo['avatar'];
            }

            $item['nickname']=$nickname;
            $item['avatar']=$avatar;

            $stream=$item['uid'].'_'.$item['showid'];
            $item['stream']=$stream;
            if(!$item['isvideo']){
                $item['pull']=Live::getStreamUrl($stream);
            }


            $classname='未分类';
            foreach ($classlist as $v){
                if($v['id']==$item['classid']){
                    $classname=$v['name'];
                    break;
                }
            }
            $item['classname']=$classname;

        }
        $count = self::getLiveWhere($where)->count();
        return compact('count', 'data');
    }


    public static function delid($id)
    {
        return self::del($id);
    }

    /**
     * 隐藏显示
     * @param $id
     * @param $show
     * @return bool
     */
    public static function setShow($id, $show)
    {
        $count = self::where('id', $id)->count();
        if (!$count) return self::setErrorInfo('参数错误');
        $count = self::where('id', $id)->where('is_show', $show)->count();
        if ($count) return true;

        self::beginTrans();
        $res = self::where('id', $id)->update(['is_show' => $show]);
        self::checkTrans($res);
        return $res;
    }

    public static function getLiveing($where)
    {
        //$classlist=LiveClass::getList();

        $data = ($data = self::getLiveWhere($where)->page((int)$where['page'], (int)$where['limit'])->select()) && count($data) ? $data->toArray() : [];
        foreach ($data as &$item) {
            $nickname='';
            $avatar='';
            $userinfo=User::getUserInfoByRedis($item['uid']);
            if($userinfo){
                $nickname=$userinfo['nickname'];
                $avatar=$userinfo['avatar'];
            }

            $item['nickname']=$nickname;
            $item['avatar']=$avatar;

            $stream=$item['uid'].'_'.$item['showid'];
            $item['stream']=$stream;
            if(!$item['isvideo']){
                $item['pull']=Live::getStreamUrl($stream);
            }
        }
        $count = self::getLiveWhere($where)->count();
        return compact('count', 'data');
    }
}