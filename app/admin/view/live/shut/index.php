{extend name="public/container"}
{block name="content"}

<div class="layui-fluid">
    <div class="layui-row layui-col-space15"  id="app">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <form class="layui-form layui-form-pane" action="">
                        <div class="layui-form-item">

                            <div class="layui-inline">
                                <label class="layui-form-label">用户ID</label>
                                <div class="layui-input-block">
                                    <input type="text" name="uid" class="layui-input" placeholder="请输入用户ID">
                                </div>
                            </div>

                            <div class="layui-inline">
                                <label class="layui-form-label">主播ID</label>
                                <div class="layui-input-block">
                                    <input type="text" name="liveuid" class="layui-input" placeholder="请输入主播ID">
                                </div>
                            </div>

                            <div class="layui-inline">
                                <div class="layui-input-inline">
                                    <button class="layui-btn layui-btn-sm layui-btn-normal" lay-submit="search" lay-filter="search">
                                        <i class="layui-icon layui-icon-search"></i>搜索</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--列表-->
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">列表</div>
                <div class="layui-card-body">
                    <div class="layui-btn-container">
                        <a class="layui-btn layui-btn-sm" href="{:Url('index')}">首页</a>
                    </div>
                    <table class="layui-hide" id="List" lay-filter="List"></table>
                    <script type="text/html" id="act">
                        <button class="layui-btn btn-danger layui-btn-xs" lay-event='delstor'>
                            <i class="fa fa-times"></i> 删除
                        </button>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{__ADMIN_PATH}js/layuiList.js"></script>
{/block}
{block name="script"}
<script>
    setTimeout(function () {
        $('.alert-info').hide();
    },3000);
    //实例化form
    layList.form.render();
    //加载列表
    layList.tableList('List',"{:Url('getlist')}",function (){
        return [
            {field: 'id', title: 'ID', width:'4%',align:'center'},
            {field: 'uid_name', title: '用户',align:'center'},
            {field: 'liveuid_name', title: '主播',align:'center'},
            {field: 'actuid_name', title: '操作者',align:'center'},
            {field: 'addtime', title: '时间',align:'center'},
            {field: 'right', title: '操作',toolbar:'#act',width:'10%',align:'center'},
        ];
    });

    layList.search('search',function(where){
        layList.reload(where,true);
    });
    //点击事件绑定
    layList.tool(function (event,data,obj) {
        switch (event) {
            case 'delstor':
                var url=layList.U({c:'live.shut',a:'delete',q:{id:data.id}});
                $eb.$swal('delete',function(){
                    $eb.axios.get(url).then(function(res){
                        if(res.status == 200 && res.data.code == 200) {
                            $eb.$swal('success',res.data.msg);
                            obj.del();
                        }else
                            return Promise.reject(res.data.msg || '删除失败')
                    }).catch(function(err){
                        $eb.$swal('error',err);
                    });
                })
                break;
            case 'open_image':
                $eb.openImage(data.thumb);
                break;
        }
    })
</script>
{/block}
