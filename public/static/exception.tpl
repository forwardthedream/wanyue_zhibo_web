<?php
if(!function_exists('parse_padding')){
        function parse_padding($source)
        {
            $length  = strlen(strval(count($source['source']) + $source['first']));
            return 40 + ($length - 1) * 8;
        }
}

if(!function_exists('parse_class')){
    function parse_class($name)
    {
        $names = explode('\\', $name);
        return '<abbr title="'.$name.'">'.end($names).'</abbr>';
    }
}

if(!function_exists('parse_file')){
    function parse_file($file, $line)
    {
        return '<a class="toggle" title="'."{$file} line {$line}".'">'.basename($file)." line {$line}".'</a>';
    }
}

if(!function_exists('parse_args')){
    function parse_args($args)
    {
        $result = [];

        foreach ($args as $key => $item) {
            switch (true) {
                case is_object($item):
                    $value = sprintf('<em>object</em>(%s)', parse_class(get_class($item)));
                break;
                case is_array($item):
                    if(count($item) > 3){
                    $value = sprintf('[%s, ...]', parse_args(array_slice($item, 0, 3)));
                    } else {
                    $value = sprintf('[%s]', parse_args($item));
                    }
                break;
                case is_string($item):
                    if(strlen($item) > 20){
                    $value = sprintf(
                    '\'<a class="toggle" title="%s">%s...</a>\'',
                    htmlentities($item),
                    htmlentities(substr($item, 0, 20))
                    );
                    } else {
                    $value = sprintf("'%s'", htmlentities($item));
                    }
                break;
                case is_int($item):
                case is_float($item):
                    $value = $item;
                break;
                case is_null($item):
                    $value = '<em>null</em>';
                break;
                case is_bool($item):
                    $value = '<em>' . ($item ? 'true' : 'false') . '</em>';
                break;
                case is_resource($item):
                    $value = '<em>resource</em>';
                break;
                default:
                    $value = htmlentities(str_replace("\n", '', var_export(strval($item), true)));
                break;
            }

            $result[] = is_int($key) ? $value : "'{$key}' => {$value}";
        }

        return implode(', ', $result);
    }
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>系统发生错误</title>
    <style>
        body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,button,textarea,p,blockquote,th,td { margin:0; padding:0; box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; -ms-box-sizing: border-box;}
        body { background:#fff; color:#555; font-size:14px; font-family: "Microsoft Yahei"; }
        a { color:#555; text-decoration:none; }
        a:hover { text-decoration:none; }
        img { border:none; }
        .clearfix:after, .clearfix:before {content: ""; display: block; height:0; clear:both; visibility: hidden;}
        .clearfix { *zoom:1; }
        .errPage{width:800px;margin:0 auto;}
        .errPage .header{margin-top:160px;}
        .errPage .header .left{width:290px;height:290px;float:left;}
        .errPage .header .left img{width:100%;height:100%;}
        .errPage .header .right{width:395px;float:right;margin-top:15px;margin-right: 50px;}
        .errPage .header .right .picture{width:228px;height:80px;}
        .errPage .header .right .picture img{width:100%;height:100%;}
        .errPage .header .right .title{font-size:20px;color:#979DAB;margin-top:26px;height:70px;}
        .errPage .header .right .question{margin-top:40px;}
        .errPage .header .right .question .btn{-moz-user-select:none;-webkit-user-select:none;-ms-user-select:none;-khtml-user-select:none;user-select:none;display:inline-block;*display:inline;zoom:0;width:116px;height:28px;border-radius:18px;background-color:#999;font-size:12px;color:#fff;text-align:center;line-height:28px;cursor:pointer;}
        .errPage .header .right .question .btn.on{background-color:#1951FC;}
        .errPage .header .right .question .btn img{width:10px;height:6px;margin-left:10px;}
        .errPage .conter{width:800px;height:424px;padding:25px 29px;background-color:#F7F7F7;border-radius:18px;overflow:auto;font-size:14px;color:#666666;margin-top:55px;line-height:2.4;display:none;}
        .source-code pre {margin: 0;}
        .source-code pre li {height: 18px;line-height: 18px;}
        .source-code pre ol{margin: 0;color: #4288ce;display: inline-block;min-width: 100%;box-sizing: border-box;font-size:14px;font-family: "Century Gothic",Consolas,"Liberation Mono",Courier,Verdana;padding-left: <?php echo (isset($source) && !empty($source)) ? parse_padding($source) : 40;  ?>px;}
        .line-error{background: #f8cbcb;}
        /* SPAN elements with the classes below are added by prettyprint. */
        pre.prettyprint .pln { color: #000 }  /* plain text */
        pre.prettyprint .str { color: #080 }  /* string content */
        pre.prettyprint .kwd { color: #008 }  /* a keyword */
        pre.prettyprint .com { color: #800 }  /* a comment */
        pre.prettyprint .typ { color: #606 }  /* a type name */
        pre.prettyprint .lit { color: #066 }  /* a literal value */
        /* punctuation, lisp open bracket, lisp close bracket */
        pre.prettyprint .pun, pre.prettyprint .opn, pre.prettyprint .clo { color: #660 }
        pre.prettyprint .tag { color: #008 }  /* a markup tag name */
        pre.prettyprint .atn { color: #606 }  /* a markup attribute name */
        pre.prettyprint .atv { color: #080 }  /* a markup attribute value */
        pre.prettyprint .dec, pre.prettyprint .var { color: #606 }  /* a declaration; a variable name */
        pre.prettyprint .fun { color: red }  /* a function name */
        h2 {color: #4288ce;font-weight: 400;padding: 6px 0;margin: 6px 0 0;font-size: 14px;border-bottom: 1px solid #eee;}
        abbr {cursor: help;text-decoration: underline;text-decoration-style: dotted;}
        h1 {margin: 10px 0 0;font-size: 28px;font-weight: 500;line-height: 32px;}
        .source-code::-webkit-scrollbar{width:10px;height:10px;}
        .source-code::-webkit-scrollbar-track{border-radius:2px;}
        .source-code::-webkit-scrollbar-thumb{background: #bfbfbf;border-radius:10px;}
        .source-code::-webkit-scrollbar-thumb:hover{background: #333;}
    </style>
</head>
<body>
<div class="errPage">
    <div class="header">
        <div class="left"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASIAAAEkCAYAAACL5zyrAAAbT0lEQVR4nO3dB7gU1d0G8Pdy6SCIoogFFLCjQix8lliJxI4YFWNDsXeN8VMTTdTYFcHeK4q9F7CjEY2xoH52xYoNsFBFgZvn//luHK+37M7OzDln5v09zz5Cwt09c3bvu2dOhYiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIazXR16+bMllvyE9WBbAugNUA9AHQE8CiADoBaO9DAaVZqwN4rYl/tAqA/gBuVFW617LoFUALA9gOwCAAmwLo5kWpJE21AK7j8yuMHCt6EG0M4AAAgwG08aA8kq1SGNmdwWjVvTtFDCL70G0D4EQAa3pQHnHLwuhalkBh5EjRgmgNABcDWM+Dsog/SmFkX1I36H3JXouCXGdHAOcBeEEhJI2wMLoGwO6qoOwVoUXUD8CtAJb3oCzit1IYWcvoer1X2cl7i2hfAM8qhKQCFkZXA9hDlZadvAaRfaOdAeByAG09KI+EpRRGe+p9y0Yeb83smq4AMMyDski4LIyuYumv0/uYrrwFUS1HPYZ6UBYJXymMaiJD/JKCvN2aXaAQkoRZGF2pFna68hRERwI40INySP6UwmgvvbfpyEsQbQjgbA/KIflVy75HhVEK8hBEXTg1v9aDski+KYxSkocgOgfAMh6UQ4pBt2kpCD2I1tcHQhxowTDaW5WfjJCDyKYeXFR/czeRjLTgbZrCKAEhB9FOXE0v4orCKCGhBpGV+3gPyiGi27QEhBpEg7mvtIgPahRG1Qk1iPbzoAwiUaUwGq5aqVyIQbQkgIEelEOkvhr2Ge2jmqlMiEH0R01eFI/VcPsZhVEFQgyi7T0og0hTFEYVCi2IbDnHAA/KIdKcUhjtq5pqXmhBtJ5uyyQgFkaXKYyaF1oQreNBGUQqUQojjfQ2IbQg6udBGUQqZWF0qcKocaEFUW8PyiASRymM9lft/VpoQdTLgzKIxGVhdInC6NdCCqIOANp5UA6RapTC6ADV4s9CCqJFPCiD5McCh1diYXSxwuhnIQVREY7HluzMcFzXCqOIkIKoowdlkPz4zoMrKYVR4U+fyfvZ9yKNmen49qzEwsh2Gj3Ig7I4oyCSopoP4DNPrt3C6MIih5GCSIrsXY+uvdBhpCCSvCqnT/FVz669FEYHe1CWTCmIJK/al3Fd4z28dgujC4oWRgoiyavFyriupzzpsK6vFEaH+FWs9CiIJK+WK+O6pgF4ydPrtzA6vyhhpCCSvFqpzOu63ePrL4XRoR6UJVUKIsmr5cu8rhs5lO8rC6NReQ8jBZHk1Wpl7ub5KYD7Pa+DUhgd5kFZUqEgkrzqWMGR5GcFUAcWRiPzGkYKIsmzzcu8tgkAxgVQD6UwOtyDsiRKQSR5tkUF13YUgB8DqAsLo/PyFkYKIsmzDQB0L/P63uAIVQhKYXREXt47BZHkmX2+d6/g+v4O4J1A6sPCaERewkhBJHm3bwWfc9saZCiAuYHUSW7CSEEkedcHwDYVXOPLgY1M5eI2TUEkRXBchddoR0WfEVi9WBgd6UE5YlEQSREMALBdhdd5PIArAqubEaGGkYJIiuJsAK0quNY6nj82IrD6GcGpCEFREElR2Nqz/63wWi2M/gTgaE+3C2nMuaGFkYJIiuSvAPrGuF77xR4I4POA6iqoMFIQSZG04Wr7OCcGP8G1a3cGVF/nskXnPQWRFM3qPEssjikAdgDw+4AmPp7DW0uvKYikiIYBOLaK6x7HW7w9ALweQP2d7XsYKYikqE4DsF8V124LZG/gvkdb8pZvlsd16XUY1UT/UjdlsruSNG81D49/kbDNZ6vmpoSuwvZA2gzAJgA2BrAqgJae1dAxDCWv+FZJIlmyHRxHA1ic+/xUy9aq3cMHOG+pF4AVAHQD0AFAW8fvcAuW5UvH5fgFBZEUXWmt1pJcCpLk/tV2+/Y2H9IE9RGJ/OTPAB4FsJTqI3sKIpGfbczV95WuS5MqKYhEfslOiL0bwH1lHtIoCVAQiTRsa84Rsu1AFlUdpUtBJNK4dlwo+wE7tMs9tFEqpCASad5C3AHRRr/GAzgQwBKqt+RoQqNIPLZFyEQG03MAXgHwHoB5qs/KaR6RSDz2Jd6fj5J5PMLaJgt+x4ePbmKHvDcURCLJsd+nZfnw2Uu+lU19RCLinIJIRJxTEImIcwoiEXFOQSQizimIRMQ5BZGIOKd5RPnxCYCveBCgbVnaG0DrgtaF7ZQ4CcAP/LLtqYWrflMQhWsBz9iyWbJPAvim3pXYe7sWgMEAhgPomvP6sHVg13D7jjca+P+XBjAIwF4A1ndQPmmCbs3C9CDP59oRwF0NhBC43OA5Hptj+yaf4vkpE3F9ztM47HifMxsJIXDpxVUANgCwBTdAE08oiMJieyAfDGCrCs/TmgHgRAADALyfo/p4koF8RYWLTccCWMfH0yyKSkEUjtk8YTTuKaVgeFkYPZ+D+rgOwOYApsb8+Xk8WueIhMslMSiIwmBbTuwG4PEESjuNuw9+GHB9jGO/148JPNco3tKJQwqiMFzCvqCk2Bnuu7LDOzTTeChiksf+/AXAv/L3sQmHgsh/XwM4PoVSTuCRyaH5G6cpJMlC7RC2PMUBBZH/Lkpxg60zAvvlswC6MqXnfgHAIyk9tzRDQeS/a1Ms4VuB3ZLcDGBuis9/fYrPLU1QEPntPc4QTtO4gOpjbMrPP063Z24oiPyWxZae3m0b2oSJKT//VC6VkYwpiPyWxS/Fp4HUxXzOok6bgsgBBZHfkpgn05wsXiMJMzN6nTT7oKQRCiK/dc6gdFm8RhKsnLUZvE77DF5D6lEQ+W3FDEq3QkD10SOD1+idwWtIPQoiv60NoFXKJQxpS4wBKT+/nUe2WMqvIQ1QEPnNzlwfmGIJW3PdWSi2Srmc2wRUF7miIPLf4SmWcCiAxQOqiyEpbvBWw4W04oCCyH+D+EiadcqeGlhdtOfWHWmwnSzXcHt5xaUgCsOoFEZzTuT2qaE5gpuhJclG5Ebm6yMTFgVRGFbkSvmkhq93SrFlkTbrvL81wc3wa1m3WYzISSMUROEYwl+Yak/m2JnPUxNwXVgwP5RAGNkBA1erk9o9BVFYdgHwTwCrxCi1HTE0AsCYnBwztDYPB+gf8+eXYJjtkXC5JAYFUXjsF/AV7svTq4zSd+CmX7aS/8jAW0L19eH+2+cB6Fbmz7RnPbyZ8tQIqcAvPpR1Uyb7XHerAXjVg3L4xlakP8Bw+oqHCnbhjGn7RdsMQNsC1MNc1sMDDKdJPHDAWn/LAOjH0ccdACziQXldOo6b4nlDByyGrx8fRdeG/WhDil4RIdKtmYg4pyASEecURCLinIJIRJxTEImIcwoiEXFOQSQizimIRMQ5BZGIOKcgEhHnFEQi4pyCSEScUxCJiHMKIhFxTkEkIs4piETEOQWRiDinIBIR5xREIuKcgkhEnFMQiYhzCiIRcU5BJCLOKYhExDkFkYg4pyASEecURCLinIJIRJxTEImIcwoiEXFOQSQizimIRMS5lnoLJKYvAHwGYCqArwFMBzADwLx6T9eOj0X4WBTA0vyzyP9TEElTLFReA/AygLf45/cBfAxgbpU11xFADwArA1gJQF8A/QGsAKBG70qxKIgkag6AZwA8yv++yP8tDTMBvMFHVBcAAwD8FsBAAGsCqNW7lG8KIrFbrHsB3AlgPIDvHdfINwDG8vEXBtMWAAYD2BJAh8K/YzmkIComa43cDuB6hs8Cj2vBgukmPqyvaWsAwwAMUkspPzRqVix2G3QggO4A9gLwhOchVJ/dJt4GYCsAPQH8A8AUv4oocSiIiuEptiCsQ/hStohCNxnACQCWAbAvO9ElUAqifHsawCYANgLwMIC6HF6tjd5dyZE3u2Wb5EGZpEIKonx6D8AQABsCeLIg12xTDa7jdICjAXzrQZmkTAqifPkBwEkAVgVwV4Hr4Fy2kMZ4UB4pg0bN8uNfvDV5K8MrWhhAH86U7sLZ0vaZag1gPh9zOfJls68/52TIrzIo25cA/ghgNIArACyZwWtKTAqi8Nkv++lsCdVfXpGUFmxl2a3e/wBYHkBvAF1jPr8tBXkHwOsA/s3HiymV/0EAq7MfaXAKzy8JUBCFzdZ57Qzg8RSuoid/cX8HYD22eJKyEGdM22MPPudMdq4/yAmWHyf4etMAbA/gcADn6HPvH/URhWsigLUTDiG7tToEwPMAPgQwknN2kgyhxnTkDOoL+NoTWJYkF8eOYrBOTbboUi0FUZge423ShwmVvnTrMplBsLbjWrFFr+uyLJ+ybKsn9Nw2irh+gnUnCVAQhed2rrmakUDJ+wG4h62r4QDaelgb7Vi2iSxr/wSe8x3ebr6ewHNJAhREYbkDwC4coq7GUgBuYAfxtoFsu1HDsr4A4KIqOspLbARvUwBvJldEiUtBFI6HORxdzciSvd+H8Zdvt0DffyvzQQDe5rq5aq7BphFsBuCjBMsnMSiIwjCbo0vVtIR6cJ+hURy1Cp11Yl8M4BGuN4vLWkb75/8j5DcFURjGc4JeXJsDeInrzvJmU17bwCqu6xEtCXFLQRSG+VWU8nDOzVk0x/XTlRupHRzz5xcEth1K7iiIwrABgPYxSnom5wIVYQMxu8YLOcO8UmtqM3+3FERhsDVdIyooaQ3//TEFrKsTAZxcwb9vx1E4cUhBFA7rUL0aQOdmStyRCz2PLHBdncC6aq5TvhdHIwdkVC5pxC/mj9RNmexzPdkv4N8A7ApgcQ/K48rXnAP0ODcBm83wWY4dtkMTmGOTF9G6+gDALACduIDXJoXuAKBNAevlOABneFCO/wopiEpa8UO0F//byo9iiQTDuyAK8dbsR071H8x9cI7iwX8iEqjQ+4hsZux5XBC5FkdNvvagXCJSgTx1Vtu6qUO5E99OnDtTzfwbEclIHkfN5kbOvrJlDcdmvH2qiFQo78P3n3FS38rc9uHKnJzpJZIrRZpH9CwP4iudcvpUTs/5EglOESc0WovoWh46aJvAn5Lw/sgiUqGiz6x+n0sCluNexrd5UCaRwil6EJUs4F49Q9mvJCIZUhD9kgXSjT4VSKQIFES/dpNvBRLJOwXRr03U6Q4i2VIQNWy0j4USySsFUcPGaI6RSHYURA37iBMeRSQDCqLGqdNaJCNZBVGbADcwu40LaEUkZVkFUR1HosbwyOSFA3hjv+FWIiKSsqyCyE4ovYUzl+2WZwpnMh/O5RW+0uRGkQxk2Ud0ceTc9pY8c3wkN4C3rV5P5WkKPvVb3a8TQEXSl+UvvZ0xfmcj/19fAMcDeA6A7eB/OYBtYh4qmCTrI7rdcRlEci/r1seoMv7NEtw36F7uSX1w/dNGMqbJjSIpyzqIJgB4uYJ/34Eb4tstUrcUy9WUpwF84ui1RQrBRX9MOa2i+uz8slcBbJ1u0Rq0QHOKRNLlIohuBTA1xs8tztu1ix30HWn0TCRFLoJoDoDLYv6s9RUdCOAFAP0SLldTXmOLTERS4Gqo/PLIUH4cK3OE7egMr0Gd1pIHdT5OSXF59v0tPAixWo8BGAbg05TLuwyAD7U+TwJkB0Y8wpUCNvDzhW+X4DKINgbwRELPZcdM75/BnB8LvU1Tfg2RJHzI0LHHeADf+1yrLoPIvASgf4LPdw2XjcxI8DmjhvOQRhHfzOf0mAf4+L+Q3iHXQbQPgCsSfk47Img39iElrTObtW1TeG6RStmdwMNs9TzEvwfJdRC14+GGXRN+3nk8OPG0KjvFG2Lbg/wh4ecUKdcbbPHczxZQ0p9vJ1x3vNpQ/tUpPK8tqj2J98bLJvzcmlMkWZrLVs9hAHoDWBXAMdxBNBchBA9aRKYHb6dapvT807leLanh99ZcwLtIQs8nUt8XbPE8yNGumXmvIR+Gou3W7L4Un78TgBu4KVsSG7L9oBX5krA6TtK1Vvw6AJbkwu+7ihBC8KRFhISH8ptiobcHb9mqsQEXw4pUYwFvue7wcW5PlnwJInAZRd8MXsfe/LMAnAjgx5jPYfX2AYCeCZdNisXWXe6s99yvWcLnZ/Q6ds3HAngWwIoxn6NOndZSpQW8FSs8eBZEozNeA7MmJ1TuH/Pnz+BohkgcN3MovvDgWRDNcTBr2bYTuRTAPQAWq/BnZ3A72xtSKpvklw27n6z392e+LeC8kE3WrG0L4BXO0aiEjaDtyT4nV2YDmMYglzDYRntv6736mU+d1SV3A9jO0WtfyWHTOA7lqSSNhftshsV3AGbxz9M5PDuHLawZ/Hez+O/m8O/f8s9zeN5a6c/RW9l+nHfSPf1qkirM4zY276kSf+ZjEG3KVe4uzOQvcty5G/14u/c9QyIaPlmwWeTjAKzgqP6keVdxjaVE+BhENVw5vIqj19+HH5ZQLcq1SAMCvoa8slv5lTj1QyJ83OSrjrc4rgx3e/lVm8ZW5f2BX0ceXaMQapiPLSJwm41JDvs7+vKs/pC15Ihg6MGaF3N5y/xx0SuiIb5ue/o95+m4kodf3nnseD/Fg7LITwMhCqFG+NoiMq14csZKDl7bjjtaivf0eXAAp0bU5uR6QmNfrH14nLo0wOeN4G0d2EHsM8qabdQ22O3lJ8pu0XbUXCNnLlMINc33Eyme4Dd5luq4/2/e+lZsS4nNOQ9JsjPHcTdDEEI4GufolPafjprHuUuHcKM2m3D2EfeFyZN/cgsTneWfnYuKvsVHOXzuI4pamqvll07wOWdz8t/dHOoOduPxGKz/a2xG264Umc2Q7wXgq6JXRHPS2p41aXZ44kBuRlbp4tSoaQydu7lyfrbXV50e+8bZkLdrG+X1Ij1woUKoPKG0iEpWZoBU0jL6mKvr787bhuMJaMvtV3YI/kr8Y+sGl+OXnzQjtOOT3wSwPof1m2KTEU8FsBbXX9l2nI8rhH7le+4QeJFn5cqD8xVC5QutRVTSgc3eYfy7bR3yPG817PGu+yIG53gA/6j/mZBYvmNrSCOUZQqlj6g+6wTci6FjI1v3AvjMryIG5zTW4RUBfy58MVIhVJlQW0SSni14mm0H1XEs37A1lNXWL7kQWh+RpO8hrt6forqOZYRCqHJqEUljludco16qobJNY2toRiDl9YZaRNKYdzlCeSaAZ7iNhTTtbIVQPGoRSbnacDqELRFZj/9dRLX3X1+x9TjLk/IERaMj7vTlIQFfcSP1SVwD5uIUk3LMZcvoGf7bGm7RsgFbTutzq4uiOjtnIdSOqxi6czcK+3M3PrrygNFxSb2YWkRu2Zt6Ajcwa839j2wr0fcjj1JITQrg9mgJtpZ+C2BdHmJZhC+7L9ga8nmblRYMk8UYJN0jf7f3bfF6f+/YxHPN4+qGL5MqnILID324k+LOTUworOOau/cbeWR5Sm65bArA2gym9fjo5GE5q/EWgD0A/NuT8ti8ur25HKp+wCTVJ3wfzwJMjILIL2ty75qBMUo1jQE1KdKKKv3Xlze2lrek0du5Hh6UK446zu4/1oPF0/Z7/Dsen75tBq3QIZxMnBgFkZ82ZyD1T6h0cxhSr3Cy4liPbvN6MJBK4dQ3gC1tP2Gr41HH5ejKZU77Z9g/l8o2ynGDqC23cp2fZGECV5twfdh7M5S3bL0Trhq7jbsTwBjugunT+9iZ/Uulvqa1PZvlPZqn+rq8FV6f4bMTRzOzdAEXkSequSBqwYseBGAdjpJ0jzT9ZnBK+wdc8f48P9h5OK2gNW+R7FunC4eqS/9t6M8n85FGOfYDcGKVezE1xjocbwVwMzefc7FHeFN6AniDJ+i6ZC2BAwHc7qgM1re2Kw9CWN1hPfwGwMtJP2ljQdSJ26buH/Me3k5qvQPADbwlCM32AM6qoLlr28qukfLUfhvF+DOAo5oZ0ajGRwykMbyNc82+CB/hkhOXHuAJwC62fO3H8Nk1xfe9XK+wPIlrKIh2A3Aue9urVccW0lGefLCbswqv/fcV/MwCtpyeyKiM3dg62oetpbRYK+QWhpKrbVWsI/h0R68Ntvj/xB0JstSOt10HenZ0+BEARqXxxPWD6GpurxHXAn5oXwLwIh8vB7AI0G6/TuItUKUjDvZzf0+pXE0pZ8g/KS+wpXQLpxBkYR1u9t/KQd2Cr70nRx2zsgLvQoZ5OGt9JoBl0uobqx9E3XkP+BuO2PTjIr6GWOi8HQmdlxg609MoaEpas+PxrwAWjvESl/DsNZeqGfKv1ALuG34LR9+mpvQ6C/GzlHQnfTnmssV5Tkaz3C1ot2PrZxOPN6Y7h10DqShn1KwLg6kfZ4++G2npzMyiBlKyLW/D4g57Xs57d186d5Me8m/Ojxy+HsM9wZP8AhrKuSpZ+5F1+FoGr9uDLXCbBtDdwbVWYhp/T5pqDbVmi65nZGBhOn9mcnMt6SLOI4rTDxRlH9YjPd3nuQU7NS/NeJTJ5ik9COAm7mekE2Ub1oKfO/sC2zKgI8B34a15fb3Zp7wlvwCbuo2ezlv8x/hZmRj9P4sURNX0A5W8yib0hOSLV7We7MDe2/HBkNN5YsoYtph0YMFPAz/D+dlb1oPyVOJ69pVF9WX/5HZV3Ep+woWzNhAwqQhBVG0/ELhboW0sf7Fnv1gWqFvzAz7Iw/2lprIvaQxX7fu6s0BaNmbn85CURzjT8jT7HkuzqFtxrtzRCS0jmcOW0el5D6KtuJF53H6g5xk+N3u28t2X1k8lPo1MB3gxkDLHZbPDr+F8tFBN4P7l0b6/o7hbRJwv9NkMnejg1hulL/Y8B9G6rLjVOPLX3DeSpf47nCH+KNdjZTVUXQ7fWz+VeIfhfjPPqsurAexDGcqugVDYgtbdm9hfaTkOXvWPDGQtFfn/ZzYQOm82tZSoKH1EtRyZ6BR5dOb8plncnOwDT/szQmz9VGJiJJQ+CqfYFWnFDl0LpW0crA8r1xx2YYyMcRu9GJeefMYvmorWL2r1vZ/y1PopVx1vByyQrg18akhTbKLiH7iH0XoezRsazy+891y8uILIL3lv/ZTr2ipn+IeiNPy9u6PJm2DwnMJ1oc7mxCmI3GvF1s++BWr9lGNHhyvds1bD/iQbXdshgyOcFrAFdBEnozrvklAQudWBH74+kX6Dtlz0CIZUacV1bWSb1Zp6IxedIwG2UGRotUOgw8bgbF4baPjcg7JkbXXO/N+Igy5J7Mc0h8PxYzmlwqeBGAVRgUQDrnXkw92S4YUGAm7hyGekU2QmcMfILNp2fG4wTEszuqMh2oJhWdIl8udoiEaf157nSQ4h+7ZHUpZquf90P+4H1puLT7uxjtpF6vNbDpNPjexv/iZHrl7R2XQiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIo0B8B/oeR7zJAZ4dQAAAABJRU5ErkJggg=="></div>
        <div class="right">
            <div class="picture"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN0AAAA4CAYAAAB5Tf0rAAARV0lEQVR4Xu1da5QcxXX+blXP7mIRZGRJOz16WcGWOeDYCYQQRHgEJAIcPbbHWiUEOcRgi0hJyMHEJwQ4YGIbc2LiGOs42GBjCBhirzW9ehCBiRPexvELgRwdHsboNT0ryRICJO3udPXN6Z1dMTv9nNbs7uyq++fMrap7v6qv3vcWtRs8XUDdj6iP0MuM/QSyHPCP+lg+82Y3vRmVLP1/9BGIXacxVHMgr+gxaXcM0VQkJgKU6+TZbKttMeWrxOgQgG84ELenlVI/eiOZInmderUiTc4pdtH2kdT3WMv7KEh3BKqdAs6iXWbL5mMNvGa1NyVds9ZMRa9GkA4MvM1Q83vM1i3Nbe6xoV1Kuuau54aQbpC/W1onizPeuI96m9vkia9dSrrmruMGkg5g0GdKpryjuU2e+NqlpGvuOg4lHdly0pD6moNWuw3tYGchM24EuN1n2f2KZYqTAeLmNntiaxdGuuo6jYNCcQMOp/UZB6n4MqGks0yN/LLS8zwHrF4A8N7a/5WQH929ll6Mr0Iq2WgEwkgXVKeN1iHNLxiBRKRzs8vm1S3E/Flv1nKFZdJ3UtDHDoGUdGOHfZySE5OufSmfKYR63lMI0RetgrwhTuGpzMggkJJuZHBtVK6JSTetk7OaraxaRRh0b8mUVzVKwTSf+hFISVc/ZqOZIjHpplzOJ7QeUgc8pGP6bqlb/tloGpGWNRyBlHTN3SISk25mJx+nbOVeBRv2MbCxZGqL45jt5lHuVwuIsJAIswGcSMBbINoB5qcOa3LT/i7yEDtO3q6MnufTwM4KEM8F470gFBm0WZTFvcUNtHdgbdphLxdE02rztEls2l2g1+OW5crNyPNMxc5lBJwE5llEaGNQDwhbFImNo7XBNFqky3aoVYIgajE6rIkHq+vNvQsq2VnOhI8QOGcLeUMtFnoHn07k/KF3uSJeLhbov979nSlr4FwBe5EDmimYdxW7M38fWU+dLHP96lJH0DkAzyIgC2AfwDsY9Athy67ihoGrjbG/pPYnJt0pndyy31Z9Pho+bpnaReGaM+U6nL9m4psA+Bw9VFIz4y0iWmP1is9hE/mV5V9MJ7foSq0BY6WvANNBEryyWNAe0g37pwBO91a2zFsFMuPUwJT8oZmtaFvD7CwmQAalYeC/JcvrdnWTu/M7Yt9okS5n2P0MZGoNUSw/sLubfpVbzFORcb7kOLyCCNoROYfPtdZlnq5Op+fVDWD+gg8oD1qm9vFKJ2obAP0LmOcOyTHww5KpLQgDU++wV4LoJpdswXL0Jsi5y9qm3YKfUTlO5SS1PzHpplzCJ7S2+UwvI0a69ot4Eh3PDxE7S+IYNiBD+KmyexftXn98T5w0umHfA+CTEbLMoL8l8CeOhnR6vnwuWHzP/9zSV4NeYnllsZsejmNLEplmIJ0Q6mRi3AdgqseGOklna3JVxlb3MtBZm1co6Tq5JWc7dzP4ijpwfEap3mVx2loY6cLsT0y6bCdPI1v5uXx83zI1DziDYxfpeS6AnY46QBgUFU9b2+nCqF4om+cziNX/xszfAeBOKY5PMtK1L+a5QnN+DrDnvDKifMVSLih9n56IqWddYmNNOoC+DuBTAPuP+vWR7imApgD8YT8QwkinG+pOgK+pC7xKJ/+UJeUF6CIVljaIdFH2JybdtI/xBzVHveJVir5umXKVn7K5PK9mVl+rG4TBBETymmKB1oSlz+bVGmL+m6RlHElH0dPLrOE8R3DOSlYW7ZjUKz74Wj3T5pgFjT3pIhStj3ShmQWRTjf4YkD95wCFEn10k2VKv+nukdyCSRdeYGLS6YadB7DWh3S+ylY2Xpxf1zEN82TNIKu0XcwJG+30Dvt5EM5MhHN1ogjStXfwhYJU1QI/QYkkr7YKdHeClKFJUtIBumG7a8Y/Ogps90lNztzZRYeD8hh90nXYj4Bwaa1CxNIodlN37e8Di2BGwXeKwAM7lu7ce6sEzWHgKoBn+BpLfJ5VyDwVBITeYW9DZSfU56OtYDxAhLeYuQOE4AV4BOkiRtSfAPQwM3oBvoQIQbu5T1qmdv5RNAzfpKF3L0ExZwHO5qKZeSZMt3iNjt4GeCOItpDDPUx0CiC+aZm0tTrvkI2UGhX454B4EoSXmXGCYFCxW/5ztVDuMp7KvQNLH79Rrs+d/hH4RYdoNjFWA+zZvR7Iz+GLrXWZx46OdF77E4x0LLJ552ZivqVWGQbsPk1O9dvm1w3b7dE/5ZOmBJZnlbrpjaH/pi7hnCbUViKc4JEnvrVUyPhcP6tI6h32PhBO9CnnV4f75RkHHqH9lf+Ycobz7cBFdhTpDPs5Avymlk9a++UCPEH2kA66Ub4TIL+1RZ/VKyfXtTMbg6GN8TKgf7VM+emjJN3Dh1mujhPWIwbp9jjAlT2mtjEKAn1p+U8g6FHfbpfkwuojiNxSnsVC/Z//up5utArytqMgna/94a49RNceKdDB8e4ZCwu6uHrLtkYh0zI1d9rp+XTDfhLAubV/EOSVRZO+Xft74A4k0besggzcmdQN+yCA93g1oOssU365+vep+YN6hlt3At6zJkSRLq9eJeYPeDsFLCoVtEeqf592KWe1Vu/tnQHqs5xb3eFENag4/zcD6RhwbyZ9Mq6HQhjpGHSAHfucnnWtL8WxX++wV4DwgI/sTyxT+wNvW1N3AfxXPm0mtOMJG+nD7G+gPx05gsXpQWdQ7UvtPxWEybWGlR358N717hA8/Mt2qOuI2M83b51laoG7n7qh3CldqwdAkudZBfJMS3XDduN/eM9vIkinG+o3AE+pLccW9rw9a9te9dhj2AcI3pEb7Py+1d3ysziNKa7M2JOOdjoHxck9PyC3A4z1hY50RNdaBfmVWBkByBnqGgbf6SP/HcvUVviQ7lqAh3XIrgyB7i+a8i+Dyg3ZvQy1v2GkI6bPFbvlzXGBiZKrHGjiGz5yj1qmdkm9QAjID+0yybPbmuxwnEk3lDt99NzGKJPM7S2Q505q1lC73JlCrd4OOWf1FFq8F8ejAAr5f+xJx7dZZubGekwIIV1vWcnpfh1zUP7ZDnULkY8HDOFuq6Bd7SFdQFtjxoZStxZ4nhxMunD7G0I6ImwoFmQHQO65V0O+RpNu6JaEt5dLcCNlJWf0Parfz9CUdO4IIS8qmvR4PQ0hkHSM56xu7ey68jLU5wH2kr5O0gFI1MFH2X+UpCNFjNuKGXFr1EFiHNDaP86TMr1w3G3alHRxEPOXCd29JMS6jE6OfDnqulpQT+848iM96yjW+mvIgpCRrmCZ2sfqQUMfY9JF2Z+QdFQCO/+hiO/bnTD0Xm4Zz2flLGPwWQTMY2DykXuLjINMsAneNWDS3me0RjoAjzO5RwXDP2IsBNA21tPLRnqOR909rIsoMe5exs1vrEkX1NaG9I/YvZTv7kSyfViwVurPoLSnC7uTTiXd44AWqR5k4I/jglgjl2jIH0XS1WXWaK/pUtLFX9M1uoOPRbpGVpBb4KwlnLPlgLd5yG3vyDabki4CorG+kRLV0/upH8fLILJlDAqM65Gu0aTLGvY6AuJ7F/ijnJIuJV0oAinpBuHJLu47lTQZHgGasReEXw9sxTN+2+9mSaOH/ERHBiG7l3F74yG5dHpZQSId6QZbRCNHuqyhbibwrf6NUjxBRDcVC/Tskd2sBp/TjdaaTmjy9/oUYvn9ubbu3Ya9Ue5K9RI5nV6O7ZFB1PQ6wd3LeptART54aik2Owfp7NrbCxPtyCAZaslSpaRLSVeZPhhqK8An1zYjAq4qmtq9tb83NenOZ00/Ufm69AcdjiejT7JUKenKnwdozA7Hm2ak0w21B2CP6z6zPLPUTR5P70aTzhZy3p615LkTmWhNV1mDHAaz59wtJR0Q1ehGevcyZ6jrGfxFn3LusUzNEzdHz6urwex6u9d+IVEQ3Due4TFigrrMUZte6oYbOYyPq1Uk6PQ+Z6jPMHiYn9Rg2tDdS92w3fALnnLg4608OALvAHimB6AoLwPDLhKg16aTwp63s/bCszsyTuEfMXNLrbxQ5eXF9W0vJxvT/FMd8yNdnXcp672rOYR605Mua9j95BM5iiGXl0zq8kwvDXU3wB7/u6jdS91Q+/1ilhDRrcWCHOaHN8PgjzpwfuH61tVLOt0obwHoVJ9mv8IytWFh5XNGeSGDfuBHEXLk7OI62pGSLjoaWFyMsnl7GTE8bcqNPKBp4qRab/CsYf+QgAu8+fPtlpn5x6Bym550uqF6AJ7uY8CzlinPqfa7Gnwz+zUAv+UjHzHSqe2+odbc99KZFxXXZZ5z86zEeHEKQQFvovzp9ADPeYB2CBZLhu4tuj57Grc9RuDfqbWFgQMlTb6vEfdWq/MOvXupyTlxG68rVwR2BemXtNGN9PQyLDiVG6FMZOTqCvHcGJrOdQT+kp9OzLS61C3vGsekK78EkG9EJwAFsLxNOdgphDqNiL7st+kSa3qZt58HB8ZIcZ/w2g7QOwz+EKEqFmMtshHTy5xRvp5BfusG1xNLAc7rgOhn8Dy/EX6wuECn33qIUSvbGNeeSq5hb443K+lwOmf02a6/o2+n7Zr1NjCwvp8VGKrBdTAW8sOltfTLcUw6212oenyZEjSu0JEua9jfIuDKBPkOTxJBuoGQElK9EUKoSBVY8QWl9Zn/iRSsU+CYJ53ryNqhvsbEq+uE7og4M35c6ta8EaerMkza6YziRspASLRNcUFgoBzQoMOvgXXYlzHhobjlBMrFCsEXduAfqcGIjHJuqSnpgOlL3mkXsu2FwfDpkZVRLeC2PXbkOT3r6MdhCZuedK7yet5+Foz5MRDYwcT3ENM/+ciGkg6XcKveql4JjghWnaN4AeD3ADzPU04M0uGzLPQX+TGwExrW2zNzBW3uV+KceryhY2B2RCQlXQWKXL68gFk8Anh3jcPx9MbT8ZMfF6R73yKe0dLiPB0S2Mi1bbeAvMhhdWaScA0VcruhzskNneY5R3sXPHqVHHEhC+W+V5D8LQOX5Mc5XwWz/7sJNbVFwMbeXnn5vk30Vj1Eqkc2Jd27aOWW8nxHOF1+oTK8mNIhBq8qmdq/x8F7XJDONWRmJ0+xbXUHgf58WAAhhrvJsUk69t/tXN/2WtLD8SGw9Hz/aXDEGtBAmLzqI4F3iPBA72F5vdvwkx6O11bK9DwvkMyrGLyYwMMe1XBDExLwrIC8Y5dJkSHk4lR46LSnk2ezrbYdbT5u+nG5kVJjuPuQCWecleCBI6j3++CyD4Lut8vi3/asJ3fXPNaXmHSxch8BITc0gzyEeexgLhP2C4XXGn1eVRn1eA6xPc8hmiyULPYzXqqe1jWKdEMQuQ+rZFowh4BZJFQbkewRElt3dtG+EYAxzbIuBFjoecwitmc5RFlA/kYRdu4R2IYu8o15U1f2MYUTxnmPmfs4EGs06caByamKY4xASroGvE83xnWYFj/OEJhwpHMDjQLwBJtVEPf3mOR52ks31C8BPqW23oLeZBhn9Zuq24QITDjSBU4XGVdb3dqwF3Lefz639Z04cHPBE4adiS8oFRp/cN2EbSBVaZQRmHiky6tvgvmqWhzdx13A4uzqdwNyhvoHBt/uh7kieVK9b46Pct2lxY1TBCYe6QLfzXNryH1XmtcS0y4H/LsEWuzvYUC7rIL0uvuM00pO1W4uBCYc6QYuu85xtoM5mxxqut0yZaBLR/J805QpAomfhm1u6HKG/Qn3qaJEWjL2l0XfqXsLkzyPgCTKL02UIlCDwMQb6QYNzBnquwxeXl+NkyIhlhXXel+SrS+fVDpFIBiBCUs6VIIH3Qfg8pgNoI9Z/kWpm74XUz4VSxFIhMDEJd0gHDMM+woHcONtBnhMkwPiR8m2P93oWCWJaiRNNOERmPCkG6jBTpbttn2eYDEf5N7FhOYQ9RDz62VHbty7nooTvqZTA5sGgf8HnZhz9eEy8dAAAAAASUVORK5CYII="></div>
            <div class="title">
                <div class="info">
                    <div>
                        <h2><?php echo nl2br(htmlentities($message)); ?></h2>
                    </div>
                    <div>
                        <h1 style="font-size: 14px;">[<?php echo $code; ?>]&nbsp;<?php echo isset($name) ? sprintf('%s in %s', parse_class($name), parse_file($file, $line)) : $exception->getMessage(); ?></h1>
                    </div>
                </div>
            </div>
            <div class="question">
                <div class="btn">查看错误代码<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABtElEQVRoQ+2Y3SoFURiGv+dmHHABTt2AM1FSkkRKUlIi2SmpnRJKJElJbWduwCGX4U6Wds3sttkz3/x8a+2xa83hNGvN87zvrFnTIBN+MOH8EgXabjA2EBswJhAfIWOA5uGxAXOExgliA0UBOuemReRYRL6AG2PQhcODNJDA90RkJrnzO7AYQsK7QA58yh1EwquAAh9MwptABfggEl4EFPiOiCwMrQXvEmYBBf4MOAm9JkwCCtw5cJTGHVKisYACdQEcZl+ZoSQaCSgwXeCgZHMb3h/Ma6K2gAJ/CeyXbVa+m6gloNz8Ctgrgw+xJioLKPDXwG5VeN8SlQQU+Ftgpy68T4lSAQX+DthuCu9LQhVQ4O+BLSu8D4lCAQX+EdjwBW+VyBVQ4J+Add/wFokRAQX+GVgLBd9U4o+AAv8CrIaGbyKRFfgWkdkM6CuwMi74ChIPwGZ63UDAOTcnIp8Z0DdgedzwZRLAgDvbwIeIzCcT9ICltuAViQ5wOtJA/4RzbkpE+p8FP0C3bfiMRO4vmtKd+L9IFHFEgbYbig3EBowJxEfIGKB5eGzAHKFxgtiAMUDz8F/6UtIxXjMNGgAAAABJRU5ErkJggg=="></div>
                <div class="btn on"><a style="color: #ffffff" href="javascript:history.back(-1)">返回上一页</a></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="conter source-code">
        <?php if(!empty($source)){?>
        <div class="source-code">
            <pre class="prettyprint lang-php"><ol start="<?php echo $source['first']; ?>"><?php foreach ((array) $source['source'] as $key => $value) { ?><li class="line-<?php echo $key + $source['first']; ?>"><code><?php echo htmlentities($value); ?></code></li><?php } ?></ol></pre>
        </div>
        <?php }?>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js"></script>
<script>

    var LINE =<?php echo isset($line) ? $line : 0; ?>;
    var down = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABuElEQVRoQ+2Y4SoEURiGn+8mXIkbcAuKkpKSlFBSkhIpSUmhLUlJbYqUW3ADfvLPVbiAV1uztYY1Z+Z8x1id+bez55x9nvc9u81ZY8QvG3F+skDbDeYGcgORCeQtFBlg9PTcQHSEkQv8vwYk7QJjQMfMXiMDcpsu6Rh4B7pm9tZf+FMDBfxe8eYLMP0XJCTd9VgKrkczmxwmoFJkrUtIugVmSlwTZvbUu1du4AxY/SsSkrrAbInn2czGv22gd1PSPTDVtoSkG2CuiuPbX6G2JSRdA/NV8F+20OCEtiQkXQELIfA/CrSxnSRdAouh8JUCvykh6QJYqgMfJPAbEpI6wHJd+GCBlBKSzoGVJvC1BFJISDoF1prC1xbwlJB0AqzHwDcS8JAoHsw2YuEbC8RISDoCNj3gowSaSEg6BLa84KMF6khIOgC2PeFdBEIkJO0DO97wbgIVEg9A75Q3eLmdM1zPxEMeAEvsuMG7NtCnrJBwhU8iEPKdKFcS89p1Cw2ClJpwT77/WckEiiaS/0WTVCBma4TOzQKhSaUalxtIlWzourmB0KRSjcsNpEo2dN3cQGhSqcaNfAMfxuz5MbcQ4poAAAAASUVORK5CYII=";
    var up = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABtElEQVRoQ+2Y3SoFURiGv+dmHHABTt2AM1FSkkRKUlIi2SmpnRJKJElJbWduwCGX4U6Wds3sttkz3/x8a+2xa83hNGvN87zvrFnTIBN+MOH8EgXabjA2EBswJhAfIWOA5uGxAXOExgliA0UBOuemReRYRL6AG2PQhcODNJDA90RkJrnzO7AYQsK7QA58yh1EwquAAh9MwptABfggEl4EFPiOiCwMrQXvEmYBBf4MOAm9JkwCCtw5cJTGHVKisYACdQEcZl+ZoSQaCSgwXeCgZHMb3h/Ma6K2gAJ/CeyXbVa+m6gloNz8Ctgrgw+xJioLKPDXwG5VeN8SlQQU+Ftgpy68T4lSAQX+DthuCu9LQhVQ4O+BLSu8D4lCAQX+EdjwBW+VyBVQ4J+Add/wFokRAQX+GVgLBd9U4o+AAv8CrIaGbyKRFfgWkdkM6CuwMi74ChIPwGZ63UDAOTcnIp8Z0DdgedzwZRLAgDvbwIeIzCcT9ICltuAViQ5wOtJA/4RzbkpE+p8FP0C3bfiMRO4vmtKd+L9IFHFEgbYbig3EBowJxEfIGKB5eGzAHKFxgtiAMUDz8F/6UtIxXjMNGgAAAABJRU5ErkJggg==";
    $(document).ready(function(){

        $.getScript = function(src, func){
            var script = document.createElement('script');
            
            script.async  = 'async';
            script.src    = src;
            script.onload = func || function(){};
            
            $('head')[0].appendChild(script);
        }

        var k = true;
        $(".question .btn").on('click',function(){
            if(k){
                $('.conter').show();
                $(this).addClass('on');
                $(this).find('img').attr('src',down);
                k = false;
            }else {
                $('.conter').hide();
                $(this).removeClass('on');
                $(this).find('img').attr('src',up);
                k = true;
            }
       });
        var ol    = $('ol', $('.prettyprint')[0]);
        // 设置出错行
        var err_line = $('.line-' + LINE, ol[0])[0];
        if(err_line) err_line.className = err_line.className + ' line-error';
        $.getScript('//cdn.bootcss.com/prettify/r298/prettify.min.js', function(){
            prettyPrint();

            // 解决Firefox浏览器一个很诡异的问题
            // 当代码高亮后，ol的行号莫名其妙的错位
            // 但是只要刷新li里面的html重新渲染就没有问题了
            if(window.navigator.userAgent.indexOf('Firefox') >= 0){
                ol[0].innerHTML = ol[0].innerHTML;
            }
        });
    });
</script>
</body>
</html>