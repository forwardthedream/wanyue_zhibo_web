<?php


namespace wanyue\interfaces;


interface ProviderInterface
{
    public function register($config);
}

