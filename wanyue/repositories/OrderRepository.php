<?php

namespace wanyue\repositories;

use app\models\store\StoreOrder;
use app\models\user\User;
use app\models\user\UserBill;
use app\admin\model\order\StoreOrder as AdminStoreOrder;
use wanyue\services\WechatAppService;

/**
 * Class OrderRepository
 * @package wanyue\repositories
 */
class OrderRepository
{
    /**
     * 微信APP支付
     * @param $orderId
     * @param string $field
     * @return array|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function AppPay($orderId, $field = 'order_id',$attach='product')
    {
		if($attach==''){
			$attach='product';
		}		
		
        if (is_string($orderId))
            $orderInfo = StoreOrder::where($field, $orderId)->find();
        else
            $orderInfo = $orderId;
        if (!$orderInfo || !isset($orderInfo['paid'])) exception('支付订单不存在!');
        if ($orderInfo['paid']) exception('支付已支付!');
        if ($orderInfo['pay_price'] <= 0) exception('该支付无需支付!');
        $bodyContent = StoreOrder::getProductTitle($orderInfo['cart_id']);
        $site_name = sys_config('site_name');
        if (!$bodyContent && !$site_name) exception('支付参数缺少：请前往后台设置->系统设置-> 填写 网站名称');
        return WechatAppService::jsPay( $orderInfo['order_id'], $orderInfo['pay_price'], $attach, StoreOrder::getSubstrUTf8($site_name . ' - ' . $bodyContent, 30), '', 'APP');
    }

    /**
     * 微信APP支付 多商户
     * @param $orderId
     * @param string $field
     * @return array|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function AppPayShop($uid, $order_id,$pay_price,$attach='product')
    {
		if($attach==''){
			$attach='product';
		}			
        $bodyContent = '支付订单';
        $site_name = sys_config('site_name');
        if (!$bodyContent && !$site_name) exception('支付参数缺少：请前往后台设置->系统设置-> 填写 网站名称');
        return WechatAppService::jsPay( $order_id, $pay_price, $attach, StoreOrder::getSubstrUTf8($site_name . ' - ' . $bodyContent, 30), '', 'APP');
    }

    /**
     * 用户确认收货
     * @param $order
     * @param $uid
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function storeProductOrderUserTakeDelivery($order, $uid)
    {
        $res1 = StoreOrder::gainUserIntegral($order);
        if (!($res1)) exception('收货失败!');
    }

    /**
     * 修改状态 为已收货  admin模块
     * @param $order
     * @throws \Exception
     */
    public static function storeProductOrderTakeDeliveryAdmin($order)
    {
        $res1 = AdminStoreOrder::gainUserIntegral($order);
        AdminStoreOrder::orderTakeAfter($order);
        UserBill::where('uid', $order['uid'])->where('link_id', $order['id'])->where('type', 'pay_money')->update(['take' => 1]);
        if (!($res1 )) exception('收货失败!');
    }

    /**
     * 修改状态 为已收货  定时任务使用
     * @param $order
     * @throws \Exception
     */
    public static function storeProductOrderTakeDeliveryTimer($order)
    {
        $res1 = AdminStoreOrder::gainUserIntegral($order, false);
        AdminStoreOrder::orderTakeAfter($order);
        UserBill::where('uid', $order['uid'])->where('link_id', $order['id'])->where('type', 'pay_money')->update(['take' => 1]);
        if (!($res1)) exception('收货失败!');
    }


    /**
     * 修改状态为  已退款  admin模块
     * @param $data
     * @param $oid
     * @return bool|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function storeProductOrderRefundY($data, $oid)
    {
        $order = AdminStoreOrder::where('id', $oid)->find();
        if ($order['is_channel'] == 1)
            return 1;
        else
            return 1;
    }


    /**
     * TODO  后台余额退款
     * @param $product
     * @param $refund_data
     * @throws \Exception
     */
    public static function storeOrderYueRefund($product, $refund_data)
    {
        $res = AdminStoreOrder::integralBack($product['id']);
        if (!$res) exception('退积分失败!');
    }

    /**
     * 订单退积分
     * @param $product $product 商品信息
     * @param $back_integral $back_integral 退多少积分
     */
    public static function storeOrderIntegralBack($product, $back_integral)
    {

    }

}